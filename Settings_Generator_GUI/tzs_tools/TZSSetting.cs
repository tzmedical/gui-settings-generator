﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Settings_Generator_GUI
{
    public class TZSSetting
    {
        public string parseStr;
        public uint min;
        public uint max;
        public uint tag;
        public ushort maxLength;
        public bool binaryFlag;
        public bool setFlag;
        public uint minFW;
        public uint maxFW;

        private uint dataVal;
        private ushort dataLength;
        private string dataStr;

        

        public TZSSetting(string pS, uint tg, uint mn, uint mx, ushort len, uint min_fw, uint max_fw)
        {
            parseStr = pS;
            tag = tg;
            min = mn;
            max = mx;
            dataVal = 0;
            maxLength = len;
            dataLength = len;
            dataStr = "";
            minFW = min_fw;
            maxFW = max_fw;
            binaryFlag = true;
            setFlag = false;
        }

        public TZSSetting(string pS, uint tg, ushort len, uint min_fw, uint max_fw)
        {
            parseStr = pS;
            tag = tg;
            min = 0;
            max = 0;
            dataVal = 0;
            maxLength = len;
            dataLength = 0;
            dataStr = "";
            minFW = min_fw;
            maxFW = max_fw;
            binaryFlag = false;
            setFlag = false;
        }

        public string getParseString()
        {
            return parseStr;
        }

        public int set(string new_value, bool check_range)
        {
            if (binaryFlag)
            {
                dataVal = ushort.Parse(new_value);
                dataStr = "";

                if (check_range && ((dataVal < min) || (dataVal > max)))
                {
                    return -1;
                }

            }
            else
            {
                dataStr = new_value;
                dataVal = 0;
                dataLength = 0;

                if (check_range && (dataStr.Length > maxLength))
                {
                    return -1;
                }
            }

            setFlag = true;
            return 0;
        }

        public uint write(byte[] buffer, uint address, bool badAlignment, uint firmwareVersion)
        {
            uint i = 0;

            if (setFlag && (firmwareVersion >= minFW) && (firmwareVersion <= maxFW))
            {
                buffer[address + i++] = (byte)tag;

                if (!binaryFlag)
                {
                    buffer[address + i++] = (byte)dataStr.Length;

                    for (int j = 0; j < dataStr.Length; j++)
                    {
                        buffer[address + i++] = (byte)dataStr[j];
                    }
                }
                else
                {
                    buffer[address + i++] = (byte)dataLength;

                    uint temp = dataVal;

                    for (int j = 0; j < dataLength; j++)
                    {
                        buffer[address + i++] = (byte)(temp & 0x000000ff);
                        temp = temp >> 8;
                    }
                }

                if (badAlignment)
                {
                    buffer[address + i++] = 0xc4;
                }
                else
                {
                    buffer[address + i++] = 0;
                }

            }

            return i;
        }

        public int checkFirmwareVersion(uint firmware_version)
        {
            if (firmware_version < minFW)
            {
                return -1;
            }
            else if (firmware_version > maxFW)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        public uint get_tag()
        {
            return tag;
        }

        public uint get_dataVal()
        {
            return dataVal;
        }

        public uint getLength()
        {
            return dataLength;
        }

        public bool isBinary()
        {
            return binaryFlag;
        }


        /***   Possible TAG values for each entry   ***/
        public enum tags
        {
            BRADY_BPM = 1,
            TACHY_BPM = 4,
            PAUSE_DURATION = 8,
            BRADY_RATE_CHANGE = 10,
            TACHY_RATE_CHANGE = 11,
            ARRHYTHMIA_MIN_DURATION = 12,
            DETECT_PACEMAKER = 31,
            SUMMARIZE_QRS_DETECTION = 40,
            PEDOMETER_PERIOD = 41,
            STORE_RAW_ACCEL = 42,
            LENGTH_IS_CALENDAR_DAYS = 95,
            ONE_ECG_FILE = 96,
            STUDY_HOURS = 97,
            SAMPLE_RATE = 101,
            REPORT_PRE_TIME = 105,
            REPORT_POST_TIME = 106,
            DIGITAL_HP_FILTER = 111,
            START_OK_CODE = 112,
            DIGITAL_LP_FILTER = 113,
            DIGITAL_NOTCH_FILTER = 114,
            MIN_TIME_BETWEEN_EVENTS = 125,
            SUPP_RHYTHM_EVENTS = 126,
            PATIENT_PRE_TIME = 127,
            PATIENT_POST_TIME = 128,
            NAG_ON_REPLACE_ELECTRODE = 144,
            NAG_ON_LOW_BATTERY = 145,
            PATCH_MAX_WEAR_LENGTH = 146,
            ALLOW_LOW_BATTERY_START = 147,
            STUDY_COMPLETE_SLEEP = 148,
            NAG_ON_LEAD_OFF = 149,
            BULK_UPLOAD = 155,
            TRANSMIT_INTERVAL_REPORTS = 156,
            SCREEN_SLEEP_TIME = 159,
            DEMO_MODE = 160,
            SYMPTOM_DIARY_COUNT = 165,
            ACTIVITY_DIARY_COUNT = 166,
            DIARY_SYMPTOM_1 = 181,
            DIARY_SYMPTOM_2 = 182,
            DIARY_SYMPTOM_3 = 183,
            DIARY_SYMPTOM_4 = 184,
            DIARY_SYMPTOM_5 = 185,
            DIARY_SYMPTOM_6 = 186,
            DIARY_SYMPTOM_7 = 187,
            DIARY_SYMPTOM_8 = 188,
            DIARY_SYMPTOM_9 = 189,
            DIARY_SYMPTOM_10 = 190,
            DIARY_ACTIVITY_1 = 191,
            DIARY_ACTIVITY_2 = 192,
            DIARY_ACTIVITY_3 = 193,
            DIARY_ACTIVITY_4 = 194,
            DIARY_ACTIVITY_5 = 195,
            DIARY_ACTIVITY_6 = 196,
            DIARY_ACTIVITY_7 = 197,
            DIARY_ACTIVITY_8 = 198,
            DIARY_ACTIVITY_9 = 199,
            DIARY_ACTIVITY_10 = 200,
            PATIENT_ID = 201,
            TOP_BANNER = 211,
            BOTTOM_BANNER = 212,
            SERVER_ADDRESS = 213,
            TLS_IDENTITY = 214,
            TLS_PSK = 215,
            APN_ADDRESS = 216,
            APN_USERNAME = 217,
            APN_PASSWORD = 218,
            HTTP_BEARER_TOKEN = 219,
            CONNECTION_TIMEOUT = 225,
            ERROR_RETRIES = 251,
            ERROR_PERIOD = 252,
            BAD_TAG = 253,
            ZYMED_COMPAT = 253,
            TZMR_COMPAT = 254,
            TERM_TAG = 255
        };

        public static TZSSetting getSetting(uint tag, uint fw, bool isPatch)
        {
            TZSSetting[] list;
            if (isPatch)
            {
                list = trident_nano_list;
            }
            else
            {
                list = trident_pro_list;
            }

            foreach (TZSSetting s in list)
            {
                if (s.get_tag() == tag && 0 == s.checkFirmwareVersion(fw))
                {
                    return s;
                }
            }

            return null;
        }

        public static TZSSetting[] trident_pro_list =
        {

            new TZSSetting(".brady_bpm=", (uint) tags.BRADY_BPM, 30, 100, 1, 11, 255),
            new TZSSetting(".tachy_bpm=", (uint) tags.TACHY_BPM, 60, 300, 2, 11, 255),
            new TZSSetting(".pause_duration=", (uint) tags.PAUSE_DURATION, 1500, 15000, 2, 11, 255),
            new TZSSetting(".brady_rate_change=", (uint) tags.BRADY_RATE_CHANGE, 0, 100, 1, 11, 255),
            new TZSSetting(".tachy_rate_change=", (uint) tags.TACHY_RATE_CHANGE, 0, 100, 1, 11, 255),
            new TZSSetting(".arrhythmia_min_duration=", (uint) tags.ARRHYTHMIA_MIN_DURATION, 0, 900, 2, 11, 255),
            new TZSSetting(".detect_pacemaker=", (uint) tags.DETECT_PACEMAKER, 0, 1, 1, 10, 255),
            new TZSSetting(".summarize_qrs_detection=", (uint) tags.SUMMARIZE_QRS_DETECTION, 0, 900, 2, 11, 255),
            new TZSSetting(".pedometer_period=", (uint) tags.PEDOMETER_PERIOD, 0, 900, 2, 11, 255),
            new TZSSetting(".one_ecg_file=", (uint) tags.ONE_ECG_FILE, 0, 1, 1, 16, 255),
            new TZSSetting(".study_hours=", (uint) tags.STUDY_HOURS, 1, 24*31, 2, 10, 255),
            new TZSSetting(".sample_rate=", (uint) tags.SAMPLE_RATE, 250, 4000, 2, 10, 255),
            new TZSSetting(".report_pre_time=", (uint) tags.REPORT_PRE_TIME, 0, 300, 2, 11, 255),
            new TZSSetting(".report_post_time=", (uint) tags.REPORT_POST_TIME, 0, 300, 2, 11, 255),
            new TZSSetting(".digital_hp_filter=", (uint) tags.DIGITAL_HP_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".start_pin_code=", (uint) tags.START_OK_CODE, 0, 9999, 2, 10, 255),
            new TZSSetting(".digital_lp_filter=", (uint) tags.DIGITAL_LP_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".digital_notch_filter=", (uint) tags.DIGITAL_NOTCH_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".min_time_between_events=", (uint) tags.MIN_TIME_BETWEEN_EVENTS, 0, 300, 2, 16, 255),
            new TZSSetting(".suppress_rhythm_events=", (uint) tags.SUPP_RHYTHM_EVENTS, 15, 19, 255),
            new TZSSetting(".suppress_nsr_events=", (uint) tags.SUPP_RHYTHM_EVENTS, 0, 1, 1, 17, 18),
            new TZSSetting(".patient_pre_time=", (uint) tags.PATIENT_PRE_TIME, 0, 300, 2, 20, 255),
            new TZSSetting(".patient_post_time=", (uint) tags.PATIENT_POST_TIME, 0, 300, 2, 20, 255),
            new TZSSetting(".allow_low_battery_start=", (uint) tags.ALLOW_LOW_BATTERY_START, 0, 1, 1, 10, 255),
            new TZSSetting(".study_complete_sleep=", (uint) tags.STUDY_COMPLETE_SLEEP, 0, 900, 2, 10, 255),
            new TZSSetting(".nag_on_lead_off=", (uint) tags.NAG_ON_LEAD_OFF, 0, 900,  2, 10, 255),
            new TZSSetting(".bulk_upload=", (uint) tags.BULK_UPLOAD, 0, 1, 1, 11, 255),
            new TZSSetting(".transmit_interval_reports=", (uint) tags.TRANSMIT_INTERVAL_REPORTS, 0, 1, 1, 11, 255),
            new TZSSetting(".screen_sleep_time=", (uint) tags.SCREEN_SLEEP_TIME, 5, 180, 1, 10, 255),
            new TZSSetting(".demo_mode=", (uint) tags.DEMO_MODE, 0, 2, 1, 10, 255),
            new TZSSetting(".symptom_diary_entries=", (uint) tags.SYMPTOM_DIARY_COUNT, 0, 10, 1, 10, 255),
            new TZSSetting(".activity_diary_entries=", (uint) tags.ACTIVITY_DIARY_COUNT, 0, 10, 1, 10, 255),
            new TZSSetting(".symptom_entry_1=", (uint) tags.DIARY_SYMPTOM_1, 22, 10, 255),
            new TZSSetting(".symptom_entry_2=", (uint) tags.DIARY_SYMPTOM_2, 22, 10, 255),
            new TZSSetting(".symptom_entry_3=", (uint) tags.DIARY_SYMPTOM_3, 22, 10, 255),
            new TZSSetting(".symptom_entry_4=", (uint) tags.DIARY_SYMPTOM_4, 22, 10, 255),
            new TZSSetting(".symptom_entry_5=", (uint) tags.DIARY_SYMPTOM_5, 22, 10, 255),
            new TZSSetting(".symptom_entry_6=", (uint) tags.DIARY_SYMPTOM_6, 22, 10, 255),
            new TZSSetting(".symptom_entry_7=", (uint) tags.DIARY_SYMPTOM_7, 22, 10, 255),
            new TZSSetting(".symptom_entry_8=", (uint) tags.DIARY_SYMPTOM_8, 22, 10, 255),
            new TZSSetting(".symptom_entry_9=", (uint) tags.DIARY_SYMPTOM_9, 22, 10, 255),
            new TZSSetting(".symptom_entry_10=", (uint) tags.DIARY_SYMPTOM_10, 22, 10, 255),
            new TZSSetting(".activity_entry_1=", (uint) tags.DIARY_ACTIVITY_1, 22, 10, 255),
            new TZSSetting(".activity_entry_2=", (uint) tags.DIARY_ACTIVITY_2, 22, 10, 255),
            new TZSSetting(".activity_entry_3=", (uint) tags.DIARY_ACTIVITY_3, 22, 10, 255),
            new TZSSetting(".activity_entry_4=", (uint) tags.DIARY_ACTIVITY_4, 22, 10, 255),
            new TZSSetting(".activity_entry_5=", (uint) tags.DIARY_ACTIVITY_5, 22, 10, 255),
            new TZSSetting(".activity_entry_6=", (uint) tags.DIARY_ACTIVITY_6, 22, 10, 255),
            new TZSSetting(".activity_entry_7=", (uint) tags.DIARY_ACTIVITY_7, 22, 10, 255),
            new TZSSetting(".activity_entry_8=", (uint) tags.DIARY_ACTIVITY_8, 22, 10, 255),
            new TZSSetting(".activity_entry_9=", (uint) tags.DIARY_ACTIVITY_9, 22, 10, 255),
            new TZSSetting(".activity_entry_10=", (uint) tags.DIARY_ACTIVITY_10, 22, 10, 255),
            new TZSSetting(".patient_id=", (uint) tags.PATIENT_ID, 40, 10, 255),
            new TZSSetting(".top_banner=", (uint) tags.TOP_BANNER, 28, 10, 255),
            new TZSSetting(".bottom_banner=", (uint) tags.BOTTOM_BANNER, 32, 19, 255),
            new TZSSetting(".server_address=", (uint) tags.SERVER_ADDRESS, 255, 11, 255),
            new TZSSetting(".tls_identity=", (uint) tags.TLS_IDENTITY, 127, 11, 255),
            new TZSSetting(".tls_psk=", (uint) tags.TLS_PSK, 127, 11, 255),
            new TZSSetting(".apn_address=", (uint) tags.APN_ADDRESS, 255, 11, 255),
            new TZSSetting(".apn_username=", (uint) tags.APN_USERNAME, 127, 11, 255),
            new TZSSetting(".apn_password=", (uint) tags.APN_PASSWORD, 127, 11, 255),
            new TZSSetting(".http_bearer_token=", (uint) tags.HTTP_BEARER_TOKEN, 127, 11, 255),
            new TZSSetting(".connection_timeout=", (uint) tags.CONNECTION_TIMEOUT, 0, 240, 1, 11, 255),
            new TZSSetting(".error_retries=", (uint) tags.ERROR_RETRIES, 0, 250, 1, 10, 255),
            new TZSSetting(".error_period=", (uint) tags.ERROR_PERIOD, 1, 1440, 2, 10, 255),
            new TZSSetting(".tzmr_compat=", (uint) tags.TZMR_COMPAT, 0, 1, 1, 12, 255),
            new TZSSetting(".bad_tag=", (uint) tags.BAD_TAG, 0, 1, 1, 10, 255)
        };

        public static TZSSetting[] trident_nano_list =
        {
            new TZSSetting(".detect_pacemaker=", (uint) tags.DETECT_PACEMAKER, 0, 1, 1, 10, 255),
            new TZSSetting(".pedometer_period=", (uint) tags.PEDOMETER_PERIOD, 0, 900, 2, 10, 255),
            new TZSSetting(".store_raw_accel=", (uint) tags.STORE_RAW_ACCEL, 0, 1, 1, 10, 255),
            new TZSSetting(".length_is_calendar_days=", (uint) tags.LENGTH_IS_CALENDAR_DAYS, 0, 1, 1, 13, 255),
            new TZSSetting(".one_ecg_file=", (uint) tags.ONE_ECG_FILE, 0, 1, 1, 11, 255),
            new TZSSetting(".study_hours=", (uint) tags.STUDY_HOURS, 1, 24*31, 2, 10, 255),
            new TZSSetting(".sample_rate=", (uint) tags.SAMPLE_RATE, 200, 1600, 2, 12, 255),
            new TZSSetting(".digital_hp_filter=", (uint) tags.DIGITAL_HP_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".digital_lp_filter=", (uint) tags.DIGITAL_LP_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".digital_notch_filter=", (uint) tags.DIGITAL_NOTCH_FILTER, 0, 100, 1, 10, 255),
            new TZSSetting(".nag_on_replace_electrode=", (uint) tags.NAG_ON_REPLACE_ELECTRODE, 0, 900,  2, 13, 255),
            new TZSSetting(".nag_on_low_battery=", (uint) tags.NAG_ON_LOW_BATTERY, 0, 900,  2, 13, 255),
            new TZSSetting(".electrode_max_wear_hours=", (uint) tags.PATCH_MAX_WEAR_LENGTH, 0, 168, 1, 13, 255),
            new TZSSetting(".allow_low_battery_start=", (uint) tags.ALLOW_LOW_BATTERY_START, 0, 1, 1, 10, 255),
            new TZSSetting(".study_complete_sleep=", (uint) tags.STUDY_COMPLETE_SLEEP, 0, 900, 2, 13, 255),
            new TZSSetting(".nag_on_lead_off=", (uint) tags.NAG_ON_LEAD_OFF, 0, 900,  2, 13, 255),
            new TZSSetting(".demo_mode=", (uint) tags.DEMO_MODE, 0, 2, 1, 10, 255),
            new TZSSetting(".patient_id=", (uint) tags.PATIENT_ID, 40, 10, 255),
            new TZSSetting(".error_retries=", (uint) tags.ERROR_RETRIES, 0, 250, 1, 10, 255),
            new TZSSetting(".error_period=", (uint) tags.ERROR_PERIOD, 1, 1440, 2, 10, 255),
            new TZSSetting(".zymed_compat=", (uint) tags.ZYMED_COMPAT, 0, 2, 1, 13, 255),
            new TZSSetting(".bad_tag=", (uint) tags.BAD_TAG, 0, 1, 1, 10, 255)
        };
    }
}
