﻿/******************************************************************************
*       Copyright (c) 2018, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*       h3r_settings_generator.cs
*
*
*
*****************************************************************************/

using System;
using System.Diagnostics;

namespace Settings_Generator_GUI
{
    class TxtToTzs
    {

        static int line_number;

        static ushort crcVal;

        /******************************************************************************
         *       Feature Controlling Definitions
         ******************************************************************************/

        private static int MAX_FILE_LENGTH = 4096;
        private static string SETTINGS_ID_STRING = "TZSET";
        private static int SERIAL_LEN = 10;

        ///////////////////////////////////////////////////////////////////////////////
        //       Private Definitions
        ///////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////
        //       Gloabal Variables
        ///////////////////////////////////////////////////////////////////////////////

        static ushort[] ccitt_crc16_table = 
        {
            0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
            0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
            0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
            0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
            0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
            0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
            0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
            0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
            0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
            0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
            0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
            0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
            0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
            0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
            0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
            0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
            0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
            0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
            0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
            0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
            0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
            0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
            0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
            0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
            0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
            0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
            0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
            0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
            0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
            0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
            0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
            0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        };


        ///////////////////////////////////////////////////////////////////////////////
        //       Private Functions
        ///////////////////////////////////////////////////////////////////////////////

        

        //-----------------------------------------------------------------------------
        //    crcBlock()
        //
        //    This function calculates the CRC-CCITT value for a block of data, given
        //    a seed value and the number of bytes to process.
        //-----------------------------------------------------------------------------
        static int crcBlock(byte[] data, uint offset, UInt32 length)
        {
            for (int j = 0; j < length; j++)
            {
                ushort crc1 = (ushort) ((crcVal >> 8) & 0x00ff);
                ushort crc2 = (ushort) ((crcVal << 8) & 0xff00);

                int address = data[j + offset] ^ crc1;
                
                crcVal = (ushort) (ccitt_crc16_table[address] ^ crc2);
            }
            return 0;
        }

        ///////////////////////////////////////////////////////////////////////////////
        //       Public Function
        ///////////////////////////////////////////////////////////////////////////////


        static public int ConvertFile(string inAddress, string outAddress, string identifier)
        {
            string[] inLines = System.IO.File.ReadAllLines(@inAddress);

            byte firmwareVersion = 0;
            string serialNumber = "";
            ushort fileID = 0;

            TZSSetting[] settingsList;

            if (identifier.Contains("HPR"))
            {
                settingsList = TZSSetting.trident_nano_list;
            }
            else
            {
                settingsList = TZSSetting.trident_pro_list;
            }

            bool checkInputData = false;
            bool badAlignment = false;
            bool badCRC = false;
            bool badLength = false;
            bool tls_id_set = false;
            bool tls_psk_set = false;
            bool tzmr_compat_set = false;

            byte[] setting_aesKey = new byte[16];
            byte[] setting_aesIV = new byte[16];

            bool setting_aesKeySet = false;
            bool setting_aesIVSet = false;
            bool setting_aesKey_detected = false;
            bool setting_aesIV_detected = false;

            // Loop through the entire input
            for (line_number = 0; line_number < inLines.Length; line_number++)
            {
                string line = inLines[line_number];

                // Trim any leading whitespace
                char[] frontTrim = { ' ', '\t' };
                line = line.TrimStart(frontTrim);

                // Trim any trailing whitespace
                char[] endTrim = { ' ', '\t', '\n', '\r'};
                line = line.TrimEnd(endTrim);

                // Skip the line if it is a comment
                if (line[0] == '#')
                {
                    continue;
                }

                // Only process lines that have more than 8 characters in them
                if (line.Length > 8)
                {
                    // Trim whitespace before or after the "="
                    bool done = false;

                    while (!done)
                    {
                        string edited = line;

                        edited = edited.Replace(" =", "=");
                        edited = edited.Replace("= ", "=");
                        edited = edited.Replace("\t=", "=");
                        edited = edited.Replace("=\t", "=");

                        if (line.Equals(edited))
                        {
                            done = true;
                        }
                        else
                        {
                            line = edited;
                        }
                    }

                    // Locate and parse any file-related parameters
                    if (line.IndexOf("file.") != -1)
                    {
                        // Sets the firmware version embedded in the header of the file
                        if (line.IndexOf("firmware_version=") != -1)
                        {
                            string tempStr = line.Substring(line.IndexOf("=") + 1);

                            if (tempStr.Length > 0)
                            {
                                firmwareVersion = byte.Parse(tempStr);
                            }
                        }

                        // Sets the serial number embedded in the header of the file
                        else if (line.IndexOf("serial_number=") != -1)
                        {
                            serialNumber = line.Substring(line.IndexOf("=") + 1);
                        }

                        // Sets the file ID embedded in the header of the file
                        else if (line.IndexOf("file_id=") != -1)
                        {
                            
                            string tempStr = line.Substring(line.IndexOf("=") + 1);
                            if (tempStr.Length > 0)
                            {

                                fileID = ushort.Parse(tempStr);
                            }
                        }
                        // Enables verification that settings are within valid ranges
                        else if (line.IndexOf("check_setting_ranges=") != -1)
                        {
                            string tempStr = line.Substring(line.IndexOf("=") + 1);

                            if (tempStr.Length > 0)
                            {
                                if (tempStr.IndexOf("true") != -1)
                                {
                                    checkInputData = true;
                                }
                                else if (tempStr.IndexOf("false") != -1)
                                {
                                    checkInputData = false;
                                }
                                else
                                {
                                    Trace.WriteLine("" + line_number + ": " + "ERROR! Invalid string: file.check_setting_ranges=" + tempStr);
                                    return -1;
                                }
                            }
                        }
                        // Enables verification that settings are within valid ranges
                        else if (line.IndexOf("insert_bad_alignment=") != -1)
                        {
                            string tempStr = line.Substring(line.IndexOf("=") + 1);

                            if (tempStr.Length > 0)
                            {
                                if (tempStr.IndexOf("true") != -1)
                                {
                                    badAlignment = true;
                                }
                                else if (tempStr.IndexOf("false") != -1)
                                {
                                    badAlignment = false;
                                }
                                else
                                {
                                    Trace.WriteLine("" + line_number + ": " + "ERROR! Invalid string: file.insert_bad_alignment=" + tempStr);
                                    return -1;
                                }
                            }
                        }
                        // Enables verification that settings are within valid ranges
                        else if (line.IndexOf("insert_bad_crc=") != -1)
                        {
                            string tempStr = line.Substring(line.IndexOf("=")+1);
                            if (tempStr.Length > 0)
                            {
                                if (tempStr.IndexOf("true") != -1)
                                {
                                    badCRC = true;
                                }
                                else if (tempStr.IndexOf("false") != -1)
                                {
                                    badCRC = false;
                                }
                                else
                                {
                                    Trace.WriteLine("" + line_number + ": " + "ERROR! Invalid string: file.insert_bad_crc=" + tempStr);
                                    return -1;
                                }
                            }
                        }
                        // Enables verification that settings are within valid ranges
                        else if (line.IndexOf("insert_bad_length=") != -1)
                        {
                            string tempStr = line.Substring(line.IndexOf("=") + 1);

                            if (tempStr.Length > 0)
                            {
                                if (tempStr.IndexOf("true") != -1)
                                {
                                    badLength = true;
                                }
                                else if (tempStr.IndexOf("false") != -1)
                                {
                                    badLength = false;
                                }
                                else
                                {
                                    Trace.WriteLine("" + line_number + ": " + "ERROR! Invalid string: file.insert_bad_length=" + tempStr);
                                    return -1;
                                }
                            }
                        }
                        // Abort if an invalid file setting is detected
                        else
                        {
                            Trace.WriteLine("" + line_number + ": " + "ERROR! Unrecognized input: " + line);
                            return -1;
                        }
                    }

                    // Locate and process any settings from the file
                    else if (line.IndexOf("setting.") != -1)
                    {

                        // Find any matches with the setting array defined at the beginning of
                        // this source file.
                        bool matchFound = false;

                        for (int i = 0; i < settingsList.Length; i++)
                        {
                            if (line.IndexOf(settingsList[i].getParseString()) != -1)
                            {
                                if (settingsList[i].set(line.Substring(line.IndexOf("=") + 1), checkInputData) != 0)
                                {
                                    return -1;
                                }

                                // flag TZMR compat mode potential conflicts
                                if ((uint) TZSSetting.tags.TZMR_COMPAT == settingsList[i].get_tag())
                                {
                                    tzmr_compat_set = (settingsList[i].get_dataVal() != 0);
                                }
                                else if ((uint) TZSSetting.tags.TLS_IDENTITY == settingsList[i].get_tag())
                                {
                                    tls_id_set = true;
                                }
                                else if ((uint) TZSSetting.tags.TLS_PSK == settingsList[i].get_tag())
                                {
                                    tls_psk_set = true;
                                }
                                matchFound = true;
                                break;
                            }
                        }

                        // We have to match a handful of settings manually.
                        if (!matchFound)
                        {

                            // Sets the AES Key stored as a setting within the file
                            if (line.IndexOf("aes_key=") != -1)
                            {
                                setting_aesKey_detected = true;
                                string tempStr =line.Substring(line.IndexOf("=") + 1);
                                if (tempStr.Length > 0)
                                {
                                    // Make sure it's the right length!
                                    if (tempStr.Length != 32)
                                    {
                                        Trace.Write("" + line_number + ": " + "ERROR! Invalid AES Key: " + tempStr);
                                        return -1;
                                    }
                                    // Check for HEX values
                                    for (int i = 0; i < 32; i++)
                                    {
                                        if ((tempStr[i] >= '0') && (tempStr[i] <= '9'))
                                        {
                                            // Digits are OK
                                        }
                                        else if ((tempStr[i] >= 'A') && (tempStr[i] <= 'F'))
                                        {
                                            // Capitals A-F are OK
                                        }
                                        else if ((tempStr[i] >= 'a') && (tempStr[i] <= 'f'))
                                        {
                                            // Lowercase a-f are OK
                                        }
                                        else
                                        {
                                            Trace.Write("" + line_number + ": " + "ERROR! Non-HEX value detected in argument: " + line);
                                            return -1;
                                        }
                                    }

                                    setting_aesKeySet = true;

                                    for (int i = 0; i < 16; i++)
                                    {
                                        string subStr = tempStr.Substring(2 * i, 2);
                                        setting_aesKey[i] = byte.Parse(subStr);
                                    }

 
                                }
                            }
                            // Sets the AES Initialization Vector stored as a setting within the file
                            else if (line.IndexOf("aes_iv=") != -1)
                            {
                                setting_aesIV_detected = true;

                                string tempStr =line.Substring(line.IndexOf("=") + 1);
                                if (tempStr.Length > 0)
                                {
                                    // Make sure it's the right length!
                                    if (tempStr.Length != 32)
                                    {
                                        Trace.Write("" + line_number + ": " + "ERROR! Invalid AES IV: " + tempStr);
                                        return -1;
                                    }
                                    // Check for HEX values
                                    for (int i = 0; i < 32; i++)
                                    {
                                        if ((tempStr[i] >= '0') && (tempStr[i] <= '9'))
                                        {
                                            // Digits are OK
                                        }
                                        else if ((tempStr[i] >= 'A') && (tempStr[i] <= 'F'))
                                        {
                                            // Capitals A-F are OK
                                        }
                                        else if ((tempStr[i] >= 'a') && (tempStr[i] <= 'f'))
                                        {
                                            // Lowercase a-f are OK
                                        }
                                        else
                                        {
                                            Trace.Write("" + line_number + ": " + "ERROR! Non-HEX value detected in argument: " + line);
                                            return -1;
                                        }
                                    }

                                    setting_aesIVSet = true;

                                    for (int i = 0; i < 16; i++)
                                    {
                                        string subStr = tempStr.Substring(2 * i, 2);
                                        setting_aesIV[i] = byte.Parse(subStr);
                                    }
                                }
                            }
                            else
                            {
                                Trace.Write("" + line_number + ": " + "ERROR! Unrecognized input: " + line);
                                return -1;
                            }
                        }
                    }
                    // Error on unrecognized settings - don't just skip them!
                    else
                    {
                        Trace.Write("" + line_number + ": " + "ERROR! Unrecognized input: " + line);
                        return -1;
                    }
                }
            }

            // Sanitary check
            if (tzmr_compat_set)
            {
                if (tls_psk_set || tls_id_set)
                {
                    Trace.Write("" + "ERROR! TLS settings and TZMR compat are not allowed concurrently");
                    return -1;
                }
            }
            else
            {
                if (setting_aesKeySet || setting_aesIVSet)
                {
                    Trace.Write("" + "ERROR! AES settings are not allowed without TZMR Compat set");
                    return -1;
                }
            }

            UInt32 length;                         // The length read from the file
            byte[] buffer = new byte[MAX_FILE_LENGTH];       // Use a big ole buffer for the file

            

            for (int i = 0; i < MAX_FILE_LENGTH; i++)
            {
                buffer[i] = (byte)0;
            }

            uint buf_addr = 6;                                   // The first 6 bytes are length and CRC

            BufWriteString(buffer, SETTINGS_ID_STRING, buf_addr, 5);  // The format identifier string
            buf_addr += 5;
            buffer[buf_addr++] = 0;

            BufWriteString(buffer, identifier, buf_addr, 5);   // The device ID string
            buf_addr += 5;
            buffer[buf_addr++] = 0;

            buffer[buf_addr++] = firmwareVersion;                // This byte is Firmware Version (*10)

            for (int i = 0; i < SERIAL_LEN; i++)
            {
                buffer[buf_addr++] = 0;
            }

            buffer[buf_addr++] = 0;
            

            buffer[buf_addr++] = (byte) ((fileID >> 0) & 0x00ff);
            buffer[buf_addr++] = (byte) ((fileID >> 8) & 0x00ff);

            for (int j = 0; j < settingsList.Length; j++)
            {
                buf_addr += settingsList[j].write(buffer, buf_addr, badAlignment, firmwareVersion);
            }

            if (setting_aesKeySet && setting_aesIVSet)
            {
                buffer[buf_addr++] = (byte) TZSSetting.tags.TLS_PSK;
                buffer[buf_addr++] = 16;

                for (int j = 0; j < 16; j++)
                {
                    buffer[buf_addr++] = (byte) setting_aesKey[j];
                }

                buffer[buf_addr++] = 0;
                buffer[buf_addr++] = (byte)TZSSetting.tags.TLS_IDENTITY;
                buffer[buf_addr++] = 16;

                for (int j = 0; j < 16; j++)
                {
                    buffer[buf_addr++] = setting_aesIV[j];
                }

                buffer[buf_addr++] = 0;
            }
            else if (setting_aesKey_detected && setting_aesIV_detected)
            {
                buffer[buf_addr++] = (byte)TZSSetting.tags.TLS_IDENTITY;
                buffer[buf_addr++] = 0;
                buffer[buf_addr++] = 0;
                buffer[buf_addr++] = (byte)TZSSetting.tags.TLS_PSK;
                buffer[buf_addr++] = 0;
                buffer[buf_addr++] = 0;
            }


            buffer[buf_addr++] = (byte)TZSSetting.tags.TERM_TAG;
            buffer[buf_addr++] = 1;
            buffer[buf_addr++] = 255;
            buffer[buf_addr++] = 0;

            length = buf_addr;

            buf_addr = 2;
            UInt32 temp = length;

            if (badLength) temp += 255;

            for (int j = 0; j < 4; j++)
            {
                buffer[buf_addr++] = (byte) (temp & 0xff);
                temp = temp >> 8;
            }

            crcVal = 0xffff;
            crcBlock(buffer, 2, length-2);


            buf_addr = 0;
            temp = crcVal;
            if (badCRC) temp++;
            for (int j = 0; j < 2; j++)
            {
                buffer[buf_addr++] = (byte) (temp & 0xff);
                temp = temp >> 8;
            }

            Array.Resize<byte>(ref buffer, (int) length);

            System.IO.File.WriteAllBytes(outAddress, buffer);

            return 0;
        }

        private static void BufWriteString(byte[] buf, string s, uint start, int length)
        {
            for (int i = 0; i < length; i++)
            {
                buf[start + i] = Convert.ToByte(s[i]);
            }
        }
    }
}
