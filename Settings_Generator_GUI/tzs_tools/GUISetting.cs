﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Settings_Generator_GUI
{
    public class GUISetting
    {
        //Shared Values
        string tzsSettingName;
        string readableName;
        string tooltip;

        Type settingType;
        Group displayGroup;

        uint minFW;
        uint maxFW;

        //Int Values
        uint minValue;
        uint maxValue;

        //String Values
        int maxLength;

        bool isMetaFlag;

        TZSSetting linkedSetting;

        //Normal Settings
        public GUISetting(TZSSetting setting, string readableName, string tooltip, Group displayGroup)
        {
            isMetaFlag = false;

            if (setting != null)
            {
                linkedSetting = setting;

                this.minFW = setting.minFW;
                this.maxFW = setting.maxFW;
                this.tzsSettingName = setting.parseStr;

                this.readableName = readableName;
                this.tooltip = tooltip;
                this.displayGroup = displayGroup;

                if (setting.binaryFlag)
                {
                    if (1 == setting.max)
                    {
                        settingType = Type.BOOLEAN;
                    }
                    else
                    {
                        settingType = Type.INT;
                        minValue = setting.min;
                        maxValue = setting.max;
                    }
                }
                else
                {
                    settingType = Type.STRING;
                    maxLength = setting.maxLength;
                }
            }
        }

        //Meta Flags
        public GUISetting(string readableName, string settingNAme, string tooltip, Group displayGroup)
        {
            isMetaFlag = true;
            settingType = Type.BOOLEAN;

            this.readableName = readableName;
            this.tooltip = tooltip;
            this.displayGroup = displayGroup;
            this.tzsSettingName = settingNAme;
        }


        public Type GetSettingType()
        {
            return settingType;
        }

        public uint GetMinValue()
        {
            return minValue;
        }

        public uint GetMaxValue()
        {
            return maxValue;
        }

        public int GetMaxLen()
        {
            return maxLength;
        }

        public string GetDisplayName()
        {
            return readableName;
        }

        public string GetName()
        {
            return tzsSettingName;
        }
        public string GetTooltip()
        {
            return tooltip;
        }

        public Group GetGroup()
        {
            return displayGroup;
        }

        public bool SettingExists()
        {
            return (null != linkedSetting);
        }

        public bool DisplaySetting()
        {
            return this.SettingExists() || this.isMetaFlag;
        }

        public enum Type
        {
            BOOLEAN = 0,
            INT = 1,
            STRING = 2
        };

        public enum Group
        {
            Basic = 0,
            Advanced,
            Notifications,
            Arrhythmia,
            Symptoms,
            Activities
        };

        public enum Device
        {
            None = 0,
            NANO_HOLTER,
            PRO_HOLTER,
            PRO_MCT
        };
    }

    public static class GUISettingsLists
    {
        public const int MIN_HPR_RTC_FW = 12;
        public const int MIN_H3R_RTC_FW = 19;

        public static List<GUISetting> GetDeviceSettings(GUISetting.Device device, uint fw)
        {
            List<GUISetting> settings = new List<GUISetting>();
            List<GUISetting> badSettings = new List<GUISetting>();

            if (device.Equals(GUISetting.Device.NANO_HOLTER)) AddNanoSettings(settings, fw);
            if (device.Equals(GUISetting.Device.PRO_HOLTER)) AddProSettings(settings, fw);
            if (device.Equals(GUISetting.Device.PRO_MCT)) AddMCTSettings(settings, fw);

            foreach (GUISetting s in settings)
            {
                if (!s.DisplaySetting())
                {
                    badSettings.Add(s);
                }
            }

            foreach (GUISetting s in badSettings)
            {
                settings.Remove(s);
            }

            return settings;
        }

        //Add all settings for the Trident Pro
        private static void AddProSettings(List<GUISetting> settingList, uint fw)
        {

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PATIENT_ID, fw, false), "Patient ID", Properties.Resources.TT_PatientId, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DETECT_PACEMAKER, fw, false), "Pacemaker Detection", Properties.Resources.TT_DetectPacemaker, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.STUDY_HOURS, fw, false), "Study Length", Properties.Resources.TT_StudyHours, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.START_OK_CODE, fw, false), "Pin Code", Properties.Resources.TT_StartPinCode, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ALLOW_LOW_BATTERY_START, fw, false), "Allow Low Bat Start", Properties.Resources.TT_AllowLowBatStart, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.TOP_BANNER, fw, false), "Top Banner Text", Properties.Resources.TT_TopBanner, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.BOTTOM_BANNER, fw, false), "Bottom Banner Text", Properties.Resources.TT_BottomBanner, GUISetting.Group.Basic));

            //Timezone "Setting"
            if (fw >= MIN_H3R_RTC_FW) settingList.Add(new GUISetting("Enable Time Zone", "-rtc_tz_en", "When this value is set, the device time zone will be synced with this computer", GUISetting.Group.Basic));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_HP_FILTER, fw, false), "High Pass Filter", Properties.Resources.TT_HPFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_LP_FILTER, fw, false), "Low Pass Filter", Properties.Resources.TT_LPFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_NOTCH_FILTER, fw, false), "Notch Filter", Properties.Resources.TT_NotchFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.SAMPLE_RATE, fw, false), "Sample Rate", Properties.Resources.TT_SampleRate, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PEDOMETER_PERIOD, fw, false), "Pedometer Report Period", Properties.Resources.TT_PedometerPeriod, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.LENGTH_IS_CALENDAR_DAYS, fw, true), "Length Is In Calendar Days", Properties.Resources.TT_LengthIsCalendarDays, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ONE_ECG_FILE, fw, false), "Single ECG File", Properties.Resources.TT_OneECGFile, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DEMO_MODE, fw, false), "Demo Mode", Properties.Resources.TT_DemoMode, GUISetting.Group.Advanced));
            //TODO: uncomment the next line when zymed compat mode is  
            //settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ZYMED_COMPAT, fw, true), "Zymed Compatibility Mode", Properties.Resources.TT_ZymedCompat, GUISetting.Group.Advanced));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.NAG_ON_LEAD_OFF, fw, false), "Lead Off Alert Period", Properties.Resources.TT_NagOnLeadOff, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.SCREEN_SLEEP_TIME, fw, false), "Screen Sleep Delay", Properties.Resources.TT_StudyCompleteSleep, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ERROR_PERIOD, fw, false), "Error Period", Properties.Resources.TT_ErrorPeriod, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ERROR_RETRIES, fw, false), "Error Retries", Properties.Resources.TT_ErrorRetries, GUISetting.Group.Notifications));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.SYMPTOM_DIARY_COUNT, fw, false), "Symptom Count", Properties.Resources.TT_SymptomCount, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_1, fw, false), "Symptom Diary 1", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_2, fw, false), "Symptom Diary 2", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_3, fw, false), "Symptom Diary 3", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_4, fw, false), "Symptom Diary 4", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_5, fw, false), "Symptom Diary 5", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_6, fw, false), "Symptom Diary 6", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_7, fw, false), "Symptom Diary 7", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_8, fw, false), "Symptom Diary 8", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_9, fw, false), "Symptom Diary 9", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_SYMPTOM_10, fw, false), "Symptom Diary 10", Properties.Resources.TT_SymptomEntry, GUISetting.Group.Symptoms));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ACTIVITY_DIARY_COUNT, fw, false), "Symptom Count", Properties.Resources.TT_ActivityCount, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_1, fw, false), "Activity Diary 1", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_2, fw, false), "Activity Diary 2", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_3, fw, false), "Activity Diary 3", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_4, fw, false), "Activity Diary 4", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_5, fw, false), "Activity Diary 5", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_6, fw, false), "Activity Diary 6", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_7, fw, false), "Activity Diary 7", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_8, fw, false), "Activity Diary 8", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_9, fw, false), "Activity Diary 9", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIARY_ACTIVITY_10, fw, false), "Activity Diary 10", Properties.Resources.TT_ActivityEntry, GUISetting.Group.Activities));
        }

        //Add all settings for the Trident MCT
        private static void AddMCTSettings(List<GUISetting> settingList, uint fw)
        {
            AddProSettings(settingList, fw);

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.BRADY_BPM, fw, false), "Bradychardia BPM", Properties.Resources.TT_BradyBPM, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.TACHY_BPM, fw, false), "Tachycardia BPM", Properties.Resources.TT_TachyBPM, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PAUSE_DURATION, fw, false), "Minimum Pause Duration", Properties.Resources.TT_PauseDur, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.BRADY_RATE_CHANGE, fw, false), "Brady Rate Change", Properties.Resources.TT_BradyRateChange, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.TACHY_RATE_CHANGE, fw, false), "Tachy Rate Change", Properties.Resources.TT_TachyRateChange, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ARRHYTHMIA_MIN_DURATION, fw, false), "Minimum Arrhythmia Duration", Properties.Resources.TT_ArrhythmiaMinDuration, GUISetting.Group.Arrhythmia));

            TZSSetting rhythmSuppression = TZSSetting.getSetting((uint)TZSSetting.tags.SUPP_RHYTHM_EVENTS, fw, false);
            if (rhythmSuppression != null && rhythmSuppression.binaryFlag)
            {
                settingList.Add(new GUISetting(rhythmSuppression, "NSR Suppression", Properties.Resources.TT_RhythmSuppOld, GUISetting.Group.Arrhythmia));
            }
            else if (rhythmSuppression != null)
            {
                settingList.Add(new GUISetting(rhythmSuppression, "Rhythm Suppression", Properties.Resources.TT_RhythmSuppNew, GUISetting.Group.Arrhythmia));
            }

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.REPORT_PRE_TIME, fw, false), "Time Before Arrhythmia", Properties.Resources.TT_ReportPreTime, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.REPORT_POST_TIME, fw, false), "Time After Arrhythmia", Properties.Resources.TT_ReportPostTime, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PATIENT_PRE_TIME, fw, false), "Time Before Patient Event", Properties.Resources.TT_PatientPreTime, GUISetting.Group.Arrhythmia));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PATIENT_POST_TIME, fw, false), "Time After Patient Event", Properties.Resources.TT_PatientPostTime, GUISetting.Group.Arrhythmia));
        }

        //Add all settings for the Trident Nano
        private static void AddNanoSettings(List<GUISetting> settingList, uint fw)
        {

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PATIENT_ID, fw, true), "Patient ID", Properties.Resources.TT_PatientId, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.STUDY_HOURS, fw, true), "Study Length", Properties.Resources.TT_StudyHours, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DETECT_PACEMAKER, fw, true), "Pacemaker Detection", Properties.Resources.TT_DetectPacemaker, GUISetting.Group.Basic));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ALLOW_LOW_BATTERY_START, fw, true), "Allow Low Bat Start", Properties.Resources.TT_AllowLowBatStart, GUISetting.Group.Basic));

            //Timezone "Setting"
            if (fw >= MIN_HPR_RTC_FW) settingList.Add(new GUISetting("Enable Time Zone", "-rtc_tz_en", "When this value is set, the device time zone will be synced with this computer", GUISetting.Group.Basic));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DEMO_MODE, fw, true), "Demo Mode", Properties.Resources.TT_DemoMode, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PEDOMETER_PERIOD, fw, true), "Pedometer Report Period", Properties.Resources.TT_PedometerPeriod, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ONE_ECG_FILE, fw, true), "Single ECG File", Properties.Resources.TT_OneECGFile, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_HP_FILTER, fw, true), "High Pass Filter", Properties.Resources.TT_HPFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_LP_FILTER, fw, true), "Low Pass Filter", Properties.Resources.TT_LPFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.DIGITAL_NOTCH_FILTER, fw, true), "Notch Filter", Properties.Resources.TT_NotchFilter, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.SAMPLE_RATE, fw, true), "Sample Rate", Properties.Resources.TT_SampleRatePatch, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.STORE_RAW_ACCEL, fw, true), "Store Accelerometer", Properties.Resources.TT_StoreRawAccel, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.LENGTH_IS_CALENDAR_DAYS, fw, true), "Length Is In Calendar Days", Properties.Resources.TT_LengthIsCalendarDays, GUISetting.Group.Advanced));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ZYMED_COMPAT, fw, true), "Zymed Compatibility Mode", Properties.Resources.TT_ZymedCompat, GUISetting.Group.Advanced));

            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.NAG_ON_LEAD_OFF, fw, true), "Lead Off Alert Period", Properties.Resources.TT_NagOnLeadOff, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.NAG_ON_LOW_BATTERY, fw, true), "Low Battery Alert Period", Properties.Resources.TT_NagOnLowBattery, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.PATCH_MAX_WEAR_LENGTH, fw, true), "Electrode Wear Length", Properties.Resources.TT_PatchWearTime, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.NAG_ON_REPLACE_ELECTRODE, fw, true), "Notify on Time Exceeded", Properties.Resources.TT_NagOnReplaceElectrode, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ERROR_PERIOD, fw, true), "Error Period", Properties.Resources.TT_ErrorPeriod, GUISetting.Group.Notifications));
            settingList.Add(new GUISetting(TZSSetting.getSetting((uint)TZSSetting.tags.ERROR_RETRIES, fw, true), "Error Retries", Properties.Resources.TT_ErrorRetries, GUISetting.Group.Notifications));
        }
    }

    public static class GUISettingGroupHelper
    { 
        // Check if a device d has settings within group g.
        public static bool HasGroup(this GUISetting.Device d, GUISetting.Group g)
        {
            bool valid = true;

            if (d == GUISetting.Device.NANO_HOLTER)
            {
                if (g == GUISetting.Group.Activities || g == GUISetting.Group.Symptoms || g == GUISetting.Group.Arrhythmia)
                {
                    valid = false;
                }
            }
            if (d == GUISetting.Device.PRO_HOLTER)
            {
                if (g == GUISetting.Group.Arrhythmia)
                {
                    valid = false;
                }
            }

            return valid;
        }

        public static bool Enabled(this GUISetting.Group g)
        {
            bool enabled = false;

            if (g.Equals(GUISetting.Group.Basic))
            {
                enabled = (bool)Properties.Settings.Default["BasicEnabled"];
            }
            else if (g.Equals(GUISetting.Group.Advanced))
            {
                enabled = (bool)Properties.Settings.Default["AdvancedEnabled"];
            }
            else if (g.Equals(GUISetting.Group.Notifications))
            {
                enabled = (bool)Properties.Settings.Default["NotificationEnabled"];
            }
            else if (g.Equals(GUISetting.Group.Arrhythmia))
            {
                enabled = (bool)Properties.Settings.Default["ArrhythmiaEnabled"];
            }
            else if (g.Equals(GUISetting.Group.Symptoms) || g.Equals(GUISetting.Group.Activities))
            {
                enabled = (bool)Properties.Settings.Default["DiaryEnabled"];
            }

            return enabled;

        }
    }
}