﻿/******************************************************************************
*       Copyright (c) 2018, TZ Medical, Inc.
*
*       All rights reserved.
*
*       Redistribution and use in source and binary forms, with or without
*       modification, are permitted provided that the following conditions
*       are met:
*
*       Redistributions of the source code must retain the above copyright
*       notice, this list of conditions, and the disclaimer below.
*
*       TZ Medical's name may not be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
*       DISCLAIMER:
*       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
*       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
*       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
*       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
*       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
*       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
*       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
*       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
*       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
*       h3r_settings_generator.cs
*
*
*
*****************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Settings_Generator_GUI
{
    class TzsToTxt
    {
        static ushort crcVal;

        /******************************************************************************
         *       Feature Controlling Definitions
         ******************************************************************************/

        private static int MAX_FILE_LENGTH = 4096;

        ///////////////////////////////////////////////////////////////////////////////
        //       Private Definitions
        ///////////////////////////////////////////////////////////////////////////////







        ///////////////////////////////////////////////////////////////////////////////
        //       Gloabal Variables
        ///////////////////////////////////////////////////////////////////////////////

        static ushort[] ccitt_crc16_table =
        {
            0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
            0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
            0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
            0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
            0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
            0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
            0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
            0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
            0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
            0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
            0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
            0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
            0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
            0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
            0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
            0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
            0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
            0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
            0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
            0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
            0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
            0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
            0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
            0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
            0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
            0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
            0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
            0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
            0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
            0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
            0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
            0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        };


        ///////////////////////////////////////////////////////////////////////////////
        //       Private Functions
        ///////////////////////////////////////////////////////////////////////////////



        //-----------------------------------------------------------------------------
        //    crcBlock()
        //
        //    This function calculates the CRC-CCITT value for a block of data, given
        //    a seed value and the number of bytes to process.
        //-----------------------------------------------------------------------------
        static int crcBlock(byte[] data, uint offset, UInt32 length)
        {
            for (int j = 0; j < length; j++)
            {
                ushort crc1 = (ushort)((crcVal >> 8) & 0x00ff);
                ushort crc2 = (ushort)((crcVal << 8) & 0xff00);

                int address = data[j + offset] ^ crc1;

                crcVal = (ushort)(ccitt_crc16_table[address] ^ crc2);
            }
            return 0;
        }

        private static void readString(byte[] buffer, int offset, out string outString)
        {
            outString = "";

            int j = offset;

            while (buffer[j] != 0)
            {
                outString = outString + (char)buffer[j++];
            }
        }

        ///////////////////////////////////////////////////////////////////////////////
        //       Public Function
        ///////////////////////////////////////////////////////////////////////////////

        static public int ConvertFile(string inAddress, string outAddress)
        {
            string temp = "";
            byte fw = 0;
            return ConvertFile(inAddress, outAddress, out temp, out fw);
        }


        static public int ConvertFile(string inAddress, string outAddress, out string device_type, out byte device_fw)
        {
            byte[] input = System.IO.File.ReadAllBytes(@inAddress);

            List<string> fileLines = new List<string>();

            TZSSetting[] settingsList;

            device_type = "";
            device_fw = 0;



            fileLines.Add("# Lines starting with \"#\" are comments");
            fileLines.Add("# This is an output file from Settings_Generator_GUI");
            fileLines.Add("# This file may be used an an input file for settings_generator.exe");

            ushort theirCrc = (ushort)(input[0] + (input[1] << 8));

            int length = input[2] +
                         (input[3] << 8) +
                         (input[4] << 16) +
                         (input[5] << 24);

            if (length > MAX_FILE_LENGTH)
            {
                return 1;
            }

            crcVal = 0xFFFF;
            crcBlock(input, 2, (uint)length - 2);

            

            if (theirCrc != crcVal)
            {
                return 1;
            }

            bool tzmr_compat_set = false;

            string identifier = "";
            string deviceID = "";
            string serialNumber = "";
            byte fwNum = 0;
            ushort fileID;

            int j = 6;

            readString(input, j, out identifier);
            j += 6;

            readString(input, j, out deviceID);
            j += 6;

            fwNum = input[j++];

            readString(input, j, out serialNumber);
            j += 11;

            fileID = (ushort)(input[j++] + (input[j++] << 8));

            fileLines.Add("file.firmware_version =" + fwNum);
            fileLines.Add("file.serial_number =" + serialNumber);
            fileLines.Add("file.file_id =" + fileID);
            fileLines.Add("file.check_setting_ranges = true");
            fileLines.Add("");

            device_fw = fwNum;
            device_type = deviceID;

            if (device_type.Contains("HPR"))
            {
                settingsList = TZSSetting.trident_nano_list;
            }
            else
            {
                settingsList = TZSSetting.trident_pro_list;
            }

            // Find if tzmr_compat is set
            uint k = (uint)j;
            while (k < (length - 2))
            {
                uint settingTag = input[k++];                                // Setting Tag
                uint settingLength = input[k++];                             // Setting Length

                if ((uint)TZSSetting.tags.TZMR_COMPAT == settingTag)
                {
                    uint temp = 0;

                    for (int i = 0; i < settingLength; i++)
                    {
                        if (i < 8)
                        {
                            temp += ((uint)input[k + i]) << (8 * i);
                        }
                    }

                    tzmr_compat_set = (temp != 0);
                    break;
                }

                k += settingLength;

                if (input[k++] != '\0')
                {
                    //"Missing NULL detected!";
                    return 1;
                }
            }

            while (j < (length - 2))
            {                                                                // Parse the events until the end of the file
                uint settingTag = input[j++];                                // Setting Tag
                uint settingLength = input[j++];                             // Setting Length

                bool matchFound = false;
                int i;

                for (i = 0; i < settingsList.Length; i++)
                {
                    if (settingTag == settingsList[i].get_tag())
                    {
                        matchFound = true;
                        break;
                    }
                }

                string settingLine = "";

                if (matchFound)
                {
                    settingLine = settingLine + "setting" + settingsList[i].getParseString();

                    if (settingsList[i].isBinary())
                    {
                        uint temp = 0;

                        for (i = 0; i < settingLength; i++)
                        {
                            if (i < 8)
                            {
                                temp += ((uint)input[j + i]) << (8 * i);
                            }
                        }

                        settingLine = settingLine + temp;
                    }
                    else
                    {
                        string tempStr = "";
                        readString(input, j, out tempStr);

                        settingLine = settingLine + tempStr;
                    }
                }
                else if (settingTag != (uint)TZSSetting.tags.TERM_TAG)
                {
                    // We need to manually handle some tags
                    if ((uint)TZSSetting.tags.TLS_PSK == settingTag)
                    {
                        if (tzmr_compat_set)
                        {
                            settingLine = settingLine + "setting.aes_key=";

                            for (k = 0; k < settingLength; k++)
                            {
                                if (k < 16)
                                {
                                    settingLine = settingLine + String.Format("{0:X2}", (int)input[j + k]);
                                }
                            }
                        }
                        else
                        {
                            string tempStr;
                            readString(input, j, out tempStr);
                            settingLine = settingLine + "setting.tls_psk=" + tempStr;
                        }
                    }
                    else if ((uint)TZSSetting.tags.TLS_IDENTITY == settingTag)
                    {
                        if (tzmr_compat_set)
                        {
                            settingLine += "setting.aes_iv=";

                            for (i = 0; i < settingLength; i++)
                            {
                                if (i < 16)
                                {
                                    settingLine = settingLine + String.Format("{0:X2}", (int)input[j + i]);
                                }
                            }
                        }
                        else
                        {
                            string tempStr;
                            readString(input, j, out tempStr);
                            settingLine = settingLine + "setting.tls_identity=" + tempStr;
                        }
                    }
                    else
                    {
                        //We don't recognize this tag. Maybe it's just from a different FW, so read through it and continue.
                        settingLine = settingLine + "#setting.[tag:" + settingTag + "]=";

                        //Make an educated guess on setting type based on length.
                        //Short settings are robably a binary value
                        if (settingLength <= 4) 
                        {
                            uint temp = 0;

                            for (i = 0; i < settingLength; i++)
                            {
                                if (i < 8)
                                {
                                    temp += ((uint)input[j + i]) << (8 * i);
                                }
                            }

                            settingLine = settingLine + temp;
                        }
                        else //Long settings are probably string values
                        {
                            string tempStr = "";
                            readString(input, j, out tempStr);

                            settingLine = settingLine + tempStr;
                        }

                        //cerr << "ERROR! Unidentified tag: " << settingTag << endl;
                    }
                }

                fileLines.Add(settingLine);
                j += (int) settingLength;

                if (input[j++] != '\0')
                {
                    return 1;
                    //cerr << "Missing NULL detected!" << endl;
                }
            }

            System.IO.File.WriteAllLines(outAddress, fileLines);

            return 0;
        }
    }
}
