﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Settings_Generator_GUI
{
    public partial class DeviceScanner : Form
    {
        // The expected name of the settings file on the SD card.
        const string DEVICE_SETTINGS_FILENAME = "config.tzs";

        // How many seconds should we wait for a drive to be accessible when
        // checking to see if it has ecg data?
        const double CHECK_DRIVE_TIMEOUT = 0.5;

        private static Mutex editDriveMutex = new Mutex();
        private static ContextMenu scannerMenu = new ContextMenu();

        private static volatile HashSet<SettingsForm> connectedDeviceForms = new HashSet<SettingsForm>();

        private static int driveCount = 0;

        public DeviceScanner()
        {
            InitializeComponent();
            scannerRunningIcon.ContextMenu = scannerMenu;
            UpdateDeviceList();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_DEVICECHANGE)
            {
                UpdateDeviceList();
            }
        }

        private static void UpdateDeviceList()
        {
            editDriveMutex.WaitOne();

            HashSet<DriveInfo> allDrives = new HashSet<DriveInfo>();
            HashSet<DriveInfo> validDrives = new HashSet<DriveInfo>();
            HashSet<DriveInfo> connectedDeviceDrives = new HashSet<DriveInfo>();
            HashSet<SettingsForm> removedDeviceForms = new HashSet<SettingsForm>();

            var driveLetters = System.Environment.GetLogicalDrives();

            foreach (string s in driveLetters)
            {
                if (Directory.Exists(s))
                {
                    allDrives.Add(new DriveInfo(s));

                    if (File.Exists(s + DEVICE_SETTINGS_FILENAME))
                    {
                        validDrives.Add(new DriveInfo(s));
                    }
                }

            }

            if (allDrives.Count != driveCount)
            {
                driveCount = allDrives.Count;

                //Get a list of devices that were connected and now aren't
                //Get a list of device drives that have already have a form
                foreach (SettingsForm f in connectedDeviceForms)
                {
                    connectedDeviceDrives.Add(f.drive);

                    if (!allDrives.ContainsDrive(f.drive))
                    {
                        removedDeviceForms.Add(f);
                    }
                }

                //Remove forms for devices that don't exist anymore.
                foreach (SettingsForm f in removedDeviceForms)
                {
                    connectedDeviceForms.Remove(f);
                    connectedDeviceDrives.Remove(f.drive);
                    f.Dispose();
                }

                //For each valid drive that doesn't have a form yet, create one.
                foreach (DriveInfo d in validDrives)
                {
                    if (!connectedDeviceDrives.ContainsDrive(d))
                    {
                        SettingsForm newDeviceForm = new SettingsForm(d);
                        connectedDeviceDrives.Add(d);
                        connectedDeviceForms.Add(newDeviceForm);
                        newDeviceForm.Show();
                    }
                }

                editDriveMutex.ReleaseMutex();

                MakeMenu();
            }
        }

        /*
         * IsValidDrive uses this to check if a drive contains ecg data. Should not
         * be called on its own as it sometimes hangs if a drive isn't cooperating.
         * @param drive The drive to check
         * @return True if the drive contains valid ecg data, false otherwise
         */
        private static Boolean IsValidDriveHelper(DriveInfo drive)
        {
            // Make sure the drive is ready (attached)
            return drive.IsReady

                // Make sure the settings file exists on the drive
                && File.Exists(System.IO.Path.Combine(drive.Name, DEVICE_SETTINGS_FILENAME));

        }

        /*
         * Checks to see if a drive contains ecg data to upload. 
         * @param drive The drive to check
         * @return True if the drive contains valid ecg data, false otherwise
         */
        private static Boolean IsValidDrive(DriveInfo drive)
        {
            bool valid = false;

            // Ask the helper function to check on this
            var task = Task.Run(() => valid = IsValidDriveHelper(drive));

            // If we don't get information about the drive after a couple seconds, 
            // consider it unresponsive and return false.
            if (!task.Wait(TimeSpan.FromSeconds(CHECK_DRIVE_TIMEOUT)))
            {
                valid = false;
            }

            return valid;
        }

        private static void MakeMenu()
        {
            List<MenuItem> menu = new List<MenuItem>();

            scannerMenu.MenuItems.Clear();
            scannerMenu.MenuItems.Add(new MenuItem("Quit", new EventHandler(MenuQuitHandler)));

            foreach (SettingsForm f in connectedDeviceForms)
            {
                scannerMenu.MenuItems.Add(new MenuItem(f.drive.VolumeLabel, new EventHandler(DeviceButtonHandler)));
            }
        }

        private static void MenuQuitHandler(Object Sender, System.EventArgs args)
        {
            if (Application.MessageLoop)
            {
                // WinForms app
                Application.Exit();
            }
            else
            {
                // Console app
                Environment.Exit(1);
            }
        }

        private static void DeviceButtonHandler(Object Sender, System.EventArgs args)
        {
            foreach (SettingsForm f in connectedDeviceForms)
            {
                if (((MenuItem) Sender).Text.Equals(f.drive.VolumeLabel) && f.IsDisposed)
                {
                    //If we remove the device from the connected forms list, it will
                    //create a new form when UpdateDeviceList is called below.
                    connectedDeviceForms.Remove(f);
                    break;
                }
            }

            UpdateDeviceList();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            // Call Dispose to remove the icon out of notification area of Taskbar.
            scannerRunningIcon.Dispose();
        }

        private const int WM_DEVICECHANGE = 0x0219;          // Detected a change in device connectivity
        private const int DBT_DEVICEARRIVAL = 0x8000;        // system detected a new device
        private const int DBT_DEVICEQUERYREMOVE = 0x8001;    // Preparing to remove (any program can disable the removal)
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004; // removed 
        private const int DBT_DEVTYP_VOLUME = 0x00000002;    // drive type is logical volume

    }

    public static class DriveSetExtension
    {

        // FWIW, This is called an Extension Method. In the code above, it's treated as though
        // it's a method in the HashSet class
        public static bool ContainsDrive(this HashSet<DriveInfo> haystack, DriveInfo needle)
        {
            foreach (DriveInfo d in haystack)
            {
                if (d.RootDirectory.FullName.Equals(needle.RootDirectory.FullName))
                    return true;
            }

            return false;
        }
    }
}
