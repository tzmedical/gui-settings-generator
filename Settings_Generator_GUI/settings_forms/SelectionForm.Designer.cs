﻿using System.Windows.Forms;

namespace Settings_Generator_GUI
{
    partial class SelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectionForm));
            this.messageString = new System.Windows.Forms.TextBox();
            this.patchButton = new System.Windows.Forms.Button();
            this.holterButton = new System.Windows.Forms.Button();
            this.mctButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // messageString
            // 
            this.messageString.BackColor = System.Drawing.SystemColors.Control;
            this.messageString.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.messageString.Enabled = false;
            this.messageString.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageString.Location = new System.Drawing.Point(12, 12);
            this.messageString.Multiline = true;
            this.messageString.Name = "messageString";
            this.messageString.ReadOnly = true;
            this.messageString.Size = new System.Drawing.Size(238, 32);
            this.messageString.TabIndex = 1;
            this.messageString.Text = "Failed to read settings file.\r\nPlease select device below.";
            // 
            // patchButton
            // 
            this.patchButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patchButton.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.patchButton.Location = new System.Drawing.Point(13, 66);
            this.patchButton.Name = "patchButton";
            this.patchButton.Size = new System.Drawing.Size(75, 23);
            this.patchButton.TabIndex = 2;
            this.patchButton.Text = "Nano";
            this.patchButton.UseVisualStyleBackColor = true;
            this.patchButton.Click += new System.EventHandler(this.patchButton_Click);
            // 
            // holterButton
            // 
            this.holterButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.holterButton.DialogResult = System.Windows.Forms.DialogResult.No;
            this.holterButton.Location = new System.Drawing.Point(96, 66);
            this.holterButton.Name = "holterButton";
            this.holterButton.Size = new System.Drawing.Size(75, 23);
            this.holterButton.TabIndex = 3;
            this.holterButton.Text = "Holter";
            this.holterButton.UseVisualStyleBackColor = true;
            this.holterButton.Click += new System.EventHandler(this.holterButton_Click);
            // 
            // mctButton
            // 
            this.mctButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mctButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.mctButton.Location = new System.Drawing.Point(177, 66);
            this.mctButton.Name = "mctButton";
            this.mctButton.Size = new System.Drawing.Size(75, 23);
            this.mctButton.TabIndex = 4;
            this.mctButton.Text = "MCT";
            this.mctButton.UseVisualStyleBackColor = true;
            this.mctButton.Click += new System.EventHandler(this.mctButton_Click);
            // 
            // SelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 101);
            this.Controls.Add(this.mctButton);
            this.Controls.Add(this.holterButton);
            this.Controls.Add(this.patchButton);
            this.Controls.Add(this.messageString);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(280, 140);
            this.MinimumSize = new System.Drawing.Size(280, 140);
            this.Name = "SelectionForm";
            this.Text = "Bad Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox messageString;
        private System.Windows.Forms.Button patchButton;
        private System.Windows.Forms.Button holterButton;
        private System.Windows.Forms.Button mctButton;
    }
}