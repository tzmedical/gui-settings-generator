﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Settings_Generator_GUI
{
    public partial class SelectionForm : Form
    {
        private int selected = 0;

        public SelectionForm()
        {
            InitializeComponent();
            patchButton.Focus();
        }


        public int ShowSelectionDialog()
        {
            while (selected == 0)
            {
                Thread.Sleep(1);
            }

            this.Close();

            return selected;
        }

        private void patchButton_Click(object sender, EventArgs e)
        {
            selected = 1;
        }

        private void holterButton_Click(object sender, EventArgs e)
        {
            selected = 2;
        }

        private void mctButton_Click(object sender, EventArgs e)
        {
            selected = 3;
        }
    }
}
