﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using Settings_Generator_GUI.settings_forms;
using System.Collections;

namespace Settings_Generator_GUI
{
    public partial class SettingsForm : Form
    {
        private GUISetting.Device device;
        private CommonOpenFileDialog backupFD;

        private int settingCount = 0;

        private List<GUISetting> deviceSettings;
        private SettingEntry[] settingForms;

        public DriveInfo drive;
        private string driveLabel;
        private string driveRoot;

        public bool IsReady = true;
        private bool dataBackedUp = false;

        private int symptom_count = 0;
        private int activity_count = 0;
        private bool enable_timezone;
        private byte fw_version;

        //DialogResults only have a few preset options.
        //Setting some consts to make code below clearer.
        private const DialogResult nanoHolterSelected = DialogResult.Yes;
        private const DialogResult proHolterSelected = DialogResult.No;
        private const DialogResult proMCTSelected = DialogResult.OK;

        private List<TabPage> settingTabs;

        public SettingsForm(int deviceIndex)
        {
            device = (GUISetting.Device)deviceIndex;
            SettingsFormInitialization(device);
        }

        public SettingsForm(DriveInfo drive)
        {
            this.drive = drive;
            driveLabel = drive.VolumeLabel;
            driveRoot = drive.RootDirectory.FullName;

            string path = drive.RootDirectory.FullName + "/config.tzs";
            string device_type = "";
            string tempFileName = @System.IO.Path.GetTempPath() + "settings.txt";

            //If we failed to parse the settings file, ask the user what device they're using.
            if (0 != TzsToTxt.ConvertFile(path, tempFileName, out device_type, out fw_version))
            {
                SelectionForm form = new SelectionForm();

                DialogResult res = form.ShowDialog();

                if (res != DialogResult.Cancel)
                {
                    if (nanoHolterSelected == res) device = GUISetting.Device.NANO_HOLTER;
                    else if (proHolterSelected == res) device = GUISetting.Device.PRO_HOLTER;
                    else if (proMCTSelected == res) device = GUISetting.Device.PRO_MCT;

                    SettingsFormInitialization(device);
                    saveFD.InitialDirectory = drive.RootDirectory.FullName;
                }
                else
                {
                    IsReady = false;
                }
            }
            else
            {
                SettingsFormInitialization((GUISetting.Device)ReadDeviceType(tempFileName, device_type));
                LoadSettings(tempFileName);
                saveFD.InitialDirectory = drive.RootDirectory.FullName;
            }

            // Modify the backupFD object so it's ready when the user goes to backup data
            backupFD = new CommonOpenFileDialog();
            backupFD.InitialDirectory = "C:\\Users";
            backupFD.IsFolderPicker = true;
            backupFD.Title = "Choose Backup Location";

            this.enabledTabsToolStripMenuItem.DropDown.Closing += this.CloseCanceller;

            //Load Preferences
            //Timezone
            if ((bool)Properties.Settings.Default["TimezoneEnabled"])
            {
                SetSetting("-rtc_tz_en", "1");
            }  
            else
            {
                SetSetting("-rtc_tz_en", "0");
            }

            //Enabled Tabs
            this.basicToolStripMenuItem.Checked = (bool) Properties.Settings.Default["BasicEnabled"];
            this.advancedToolStripMenuItem.Checked = (bool) Properties.Settings.Default["AdvancedEnabled"];
            this.notificationToolStripMenuItem.Checked = (bool) Properties.Settings.Default["NotificationEnabled"];
            this.patientDiaryToolStripMenuItem.Checked = (bool)Properties.Settings.Default["DiaryEnabled"];
            this.arrhythmiaToolStripMenuItem.Checked = (bool) Properties.Settings.Default["ArrhythmiaEnabled"];
        }

        private void SettingsFormInitialization(GUISetting.Device deviceType)
        {
            device = deviceType;
            InitializeComponent();
            Text = "Trident Settings Generator " + git_version.get_str();

            settingTabs = new List<TabPage>();

            int numTabs = 0;
            deviceSettings = GUISettingsLists.GetDeviceSettings(device, fw_version);
            settingForms = new SettingEntry[deviceSettings.Count];

            //int settingsAdded = 0;

            this.SuspendLayout();

            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip.AutoPopDelay = 5000;
            toolTip.InitialDelay = 500;
            toolTip.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip.ShowAlways = true;

            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(fileDrag_enter);
            this.DragDrop += new DragEventHandler(fileDrag_drop);

            foreach (GUISetting.Group group in Enum.GetValues(typeof(GUISetting.Group)))
            {
                if (device.HasGroup(group))
                {
                    //Add the new tab
                    TabPage settingsTab = new TabPage
                    {
                        Location = new System.Drawing.Point(2, 22),
                        Name = Enum.GetName(typeof(GUISetting.Group), group),
                        Padding = new System.Windows.Forms.Padding(1),
                        Size = new System.Drawing.Size(400, 368),
                        TabIndex = numTabs,
                        Text = Enum.GetName(typeof(GUISetting.Group), group),
                        UseVisualStyleBackColor = true,
                        AutoScroll = true,
                        BackColor = SystemColors.Control,
                    };

                    settingTabs.Add(settingsTab);
                    //tabControlSettings.Controls.Add(settingsTab);

                    int tabCount = 0;

                    if (group == GUISetting.Group.Symptoms || group == GUISetting.Group.Activities)
                    {
                        //Add the settings from that tab
                        for (int i = 0; i < deviceSettings.Count; i++)
                        {
                            GUISetting s = deviceSettings[i];

                            if (s.GetGroup() == group)
                            {
                                Control entry;
                                TextBox title;

                                if (s.GetSettingType() == GUISetting.Type.INT)
                                {
                                    title = new System.Windows.Forms.TextBox
                                    {
                                        BackColor = System.Drawing.SystemColors.Control,
                                        BorderStyle = System.Windows.Forms.BorderStyle.None,
                                        Location = new System.Drawing.Point(10, 16),
                                        Name = "titleBox" + settingCount,
                                        Text = s.GetDisplayName(),
                                        Size = new System.Drawing.Size(100, 12),
                                        ReadOnly = true,
                                        Anchor = System.Windows.Forms.AnchorStyles.Left |
                                                System.Windows.Forms.AnchorStyles.Top
                                    };

                                    entry = new System.Windows.Forms.NumericUpDown
                                    {
                                        Location = new System.Drawing.Point(150, 12),
                                        Name = "entryBox" + settingCount,
                                        Size = new System.Drawing.Size(50, 20),
                                        Maximum = s.GetMaxValue(),
                                        Minimum = s.GetMinValue(),
                                        ReadOnly = false,
                                    };

                                    if (group == GUISetting.Group.Symptoms)
                                    {
                                        ((NumericUpDown)entry).ValueChanged += SymptomCountChanged;
                                    }
                                    else
                                    {
                                        ((NumericUpDown)entry).ValueChanged += ActivityCountChanged;
                                    }
                                }
                                else
                                {
                                    title = new System.Windows.Forms.TextBox
                                    {
                                        BackColor = System.Drawing.SystemColors.Control,
                                        BorderStyle = System.Windows.Forms.BorderStyle.None,
                                        Location = new System.Drawing.Point(10, 16 + 24 * tabCount),
                                        Name = "titleBox" + settingCount,
                                        Text = s.GetDisplayName(),
                                        Size = new System.Drawing.Size(140, 12),
                                        ReadOnly = true,
                                        Anchor = System.Windows.Forms.AnchorStyles.Left |
                                                 System.Windows.Forms.AnchorStyles.Top
                                    };

                                    entry = new System.Windows.Forms.TextBox
                                    {
                                        Location = new System.Drawing.Point(150, 12 + 24 * tabCount),
                                        Name = "entryBox" + settingCount,
                                        Size = new System.Drawing.Size(175, 20),
                                        MaxLength = s.GetMaxLen(),
                                        ReadOnly = false,

                                    };
                                }

                                settingForms[i] = new SettingEntry(title, entry);

                                settingsTab.Controls.Add(title);
                                settingsTab.Controls.Add(entry);

                                toolTip.SetToolTip(entry, s.GetTooltip());

                                tabCount++;
                                settingCount++;
                            }
                        }

                    }
                    else
                    {
                        //Add the settings from that tab
                        for (int i = 0; i < deviceSettings.Count; i++)
                        {
                            GUISetting s = deviceSettings[i];

                            if (s.GetGroup() == group)
                            {
                                Control entry;
                                TextBox title;
                                TextBox extra;

                                title = new System.Windows.Forms.TextBox
                                {
                                    BackColor = System.Drawing.SystemColors.Control,
                                    BorderStyle = System.Windows.Forms.BorderStyle.None,
                                    Location = new System.Drawing.Point(10, 16 + 24 * tabCount),
                                    Name = "titleBox" + settingCount,
                                    Text = s.GetDisplayName(),
                                    Size = new System.Drawing.Size(140, 12),
                                    ReadOnly = true,
                                    Anchor = System.Windows.Forms.AnchorStyles.Left |
                                             System.Windows.Forms.AnchorStyles.Top
                                };


                                if (s.GetSettingType() == GUISetting.Type.STRING)
                                {
                                    entry = new System.Windows.Forms.TextBox
                                    {
                                        Location = new System.Drawing.Point(150, 12 + 24 * tabCount),
                                        Name = "entryBox" + settingCount,
                                        Size = new System.Drawing.Size(175, 20),
                                        MaxLength = s.GetMaxLen(),
                                        ReadOnly = false,

                                    };

                                    extra = new System.Windows.Forms.TextBox
                                    {
                                        BackColor = System.Drawing.SystemColors.Control,
                                        ForeColor = System.Drawing.SystemColors.ControlDark,
                                        BorderStyle = System.Windows.Forms.BorderStyle.None,
                                        Location = new System.Drawing.Point(330, 14 + 24 * tabCount),
                                        Name = "extraBox" + settingCount,
                                        Text = "",
                                        Size = new System.Drawing.Size(65, 12),
                                        ReadOnly = true,
                                    };
                                }
                                else if (s.GetSettingType() == GUISetting.Type.INT)
                                {
                                    entry = new System.Windows.Forms.TextBox
                                    {
                                        Location = new System.Drawing.Point(150, 12 + 24 * tabCount),
                                        Name = "entryBox" + settingCount,
                                        Size = new System.Drawing.Size(75, 20),
                                        ReadOnly = false,
                                    };

                                    extra = new System.Windows.Forms.TextBox
                                    {
                                        BackColor = System.Drawing.SystemColors.Control,
                                        ForeColor = System.Drawing.SystemColors.ControlDark,
                                        BorderStyle = System.Windows.Forms.BorderStyle.None,
                                        Location = new System.Drawing.Point(230, 14 + 24 * tabCount),
                                        Name = "extraBox" + settingCount,
                                        Text = "(" + s.GetMinValue() + "-" + s.GetMaxValue() + ")",
                                        Size = new System.Drawing.Size(65, 12),
                                        ReadOnly = true,
                                    };
                                }
                                else
                                {
                                    entry = new System.Windows.Forms.CheckBox
                                    {
                                        Location = new System.Drawing.Point(150, 12 + 24 * tabCount),
                                        Name = "entryBox" + settingCount,
                                        Size = new System.Drawing.Size(20, 20),
                                    };

                                    ((CheckBox)entry).CheckState = CheckState.Indeterminate;

                                    extra = new System.Windows.Forms.TextBox
                                    {
                                        BackColor = System.Drawing.SystemColors.Control,
                                        ForeColor = System.Drawing.SystemColors.ControlDark,
                                        BorderStyle = System.Windows.Forms.BorderStyle.None,
                                        Location = new System.Drawing.Point(45, 16 + 24 * tabCount),
                                        Name = "extraBox" + settingCount,
                                        Text = "",
                                        Size = new System.Drawing.Size(100, 12),
                                        ReadOnly = true,
                                        Anchor = System.Windows.Forms.AnchorStyles.Left |
                                                 System.Windows.Forms.AnchorStyles.Top
                                    };
                                }

                                settingForms[i] = new SettingEntry(title, entry);

                                settingsTab.Controls.Add(title);
                                settingsTab.Controls.Add(entry);
                                settingsTab.Controls.Add(extra);

                                toolTip.SetToolTip(entry, s.GetTooltip());

                                tabCount++;
                                settingCount++;
                            }
                        }
                    }

                    numTabs++;
                }
            }

            ReloadTabs();
            ResumeLayout(false);
            PerformLayout();

            settingForms[0].Entry.Select();
            settingForms[0].Entry.Focus();


        }

        private void LoadSettings(String SettingsPath)
        {
            string[] lines = System.IO.File.ReadAllLines(SettingsPath);

            for (int i = 0; i < lines.Length; i++)
            {
                string s = lines[i];

                s = s.TrimStart(" \t".ToCharArray());
                s = s.TrimEnd(" \t\n".ToCharArray());

                while (s.Contains(" =")) { s = s.Replace(" =", "="); }
                while (s.Contains("\t=")) { s = s.Replace("\t=", "="); }
                while (s.Contains("= ")) { s = s.Replace("= ", "="); }
                while (s.Contains("=\t")) { s = s.Replace("=\t", "="); }

                if (s.StartsWith("setting"))
                {
                    string tag;
                    string val;
                    int split;

                    s = s.Replace("setting.", ".");

                    split = s.IndexOf('=') + 1;

                    if (split == -1)
                    {
                        continue;
                    }

                    tag = s.Substring(0, split);
                    val = s.Substring(split);

                    SetSetting(tag, val);

                }
                else if (s.StartsWith("file.serial_number="))
                {
                    //Hack for HPR serial numbers returning 1 extra character. Fixed in HPR FW 1.1
                    string serial = s.Substring(s.IndexOf("=") + 1).Replace("¥", "");

                    deviceSerial.Text = "Device Settings: " + serial;

                }
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Path.Combine(driveRoot, "ecgs")))
            {
                BackupDeviceFiles();
            }
            else
            {
                MessageBox.Show("There is no Patient Data on the device.", "Can't Backup Data", MessageBoxButtons.OK);
            }
            
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
#if (SKIP_BACKUP)
            DialogResult dialogResult = MessageBox.Show(
                "Would you like to remove old patient data from the device?", 
                "Patient Data", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                CopyFormatAction();
            }
#else
            BackupDeviceFiles();
#endif

            string filename = driveRoot + "config.tzs";
            SaveSettings(filename);

            if ( (device == GUISetting.Device.NANO_HOLTER && fw_version >= GUISettingsLists.MIN_HPR_RTC_FW) 
              || (device == GUISetting.Device.PRO_HOLTER && fw_version >= GUISettingsLists.MIN_H3R_RTC_FW)
              || (device == GUISetting.Device.PRO_MCT && fw_version >= GUISettingsLists.MIN_H3R_RTC_FW))
            {
                string time_filename = driveRoot + "time.rtc";

                TimeZoneInfo localZone = TimeZoneInfo.Local;

                Int16 timezone_offset = 0x7fff;

                if (enable_timezone)
                {
                    timezone_offset = (Int16)localZone.BaseUtcOffset.TotalMinutes;
                }
                 
                byte[] timezone_bytes = BitConverter.GetBytes(timezone_offset);
                System.IO.File.WriteAllBytes(time_filename, timezone_bytes);
            }

            DialogResult ejectResult = AutoClosingMessageBox.Show("The device settings have been updated.", "Device Updated", 3000, MessageBoxButtons.OK);

            if (ejectResult != DialogResult.No)
            {
                EjectDrive();
            }
        }

        private void fileDrag_enter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void fileDrag_drop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            if (files.Length > 1)
            {
                MessageBox.Show("Please only place one (1) *.tzs file to import settings.", "Too Many Inputs", MessageBoxButtons.OK);
            }
            else if (files[0].EndsWith(".tzs"))
            {
                string device_type = "";
                string tempFileName = @System.IO.Path.GetTempPath() + "settings.txt";

                if (0 == TzsToTxt.ConvertFile(files[0], tempFileName, out device_type, out fw_version))
                {
                    LoadSettings(tempFileName);
                }
                else
                {
                    MessageBox.Show("There was a problem with the settings file you tried to import.", "Invalid Input File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please place a *.tzs file to import settings.", "Invalid File Type", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BackupDeviceFiles()
        {
            //Check for patient data
            if (Directory.Exists(Path.Combine(driveRoot, "ecgs")) && !dataBackedUp)
            {
                //Show the FolderBrowserDialog

                bool validBackup = false;

                //Get the path to store data
                if ((string)Properties.Settings.Default["ECGBackupLocation"] == null)
                {
                    string defaultPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Trident Utils", "ECG Backup");
                    Directory.CreateDirectory(defaultPath);

                    backupFD.InitialDirectory = defaultPath;
                }
                else
                {
                    backupFD.InitialDirectory = (string)Properties.Settings.Default["ECGBackupLocation"];
                }

                CommonFileDialogResult result = CommonFileDialogResult.None;

                while (!validBackup)
                {
                    result = backupFD.ShowDialog();

                    if (result == CommonFileDialogResult.Ok)
                    {
                        if (!backupFD.FileName.StartsWith(driveRoot))
                        {
                            validBackup = true;
                        }
                        else
                        {
                            MessageBox.Show("Can't backup to device!", "Invalid Backup Location",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        //don't get stuck in a retry loop if they don't want to back up the data.
                        validBackup = true;
                    }
                }

                if (result == CommonFileDialogResult.Ok)
                {
                    String folderPath = backupFD.FileName;
                    Properties.Settings.Default["ECGBackupLocation"] = folderPath;
                    Properties.Settings.Default.Save();

                    folderPath = Path.Combine(folderPath, driveLabel + "_" + DateTime.Now.ToString("yyyyMMddHHmm"));
                    Directory.CreateDirectory(folderPath);

                    FileCopyDialog copyDialog = new FileCopyDialog(driveRoot, folderPath);

                    // Show the file copy progress window. May need to be fixed for large studies (> 1 week);
                    if (copyDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        dataBackedUp = true;

                        DialogResult formatResult = MessageBox.Show("Succesfully backed up data! Would you like to remove patient data from the device?", "Backup and Format", MessageBoxButtons.YesNo);

                        if (formatResult == DialogResult.Yes)
                        {
                            //Only add the format action if we succesfully backed up the data.
                            //Select the correct format action file for the device.
                            CopyFormatAction();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Canceled!");
                    }
                    copyDialog.Dispose();
                }
            }
        }

        private void CopyFormatAction()
        {
            String actionFile = "h3r_reformat.tza";

            if (device.Equals(GUISetting.Device.NANO_HOLTER))
            {
                actionFile = "hpr_reformat.tza";
            }

            File.Copy(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, actionFile), Path.Combine(driveRoot, actionFile), true);
        }
        

        private GUISetting.Device ReadDeviceType(string path, string identifier)
        {
            int i = 0;
            bool mctFound = false;
            string[] fileLines = System.IO.File.ReadAllLines(path);
            device = GUISetting.Device.PRO_HOLTER;

            if (identifier.Contains("HPR"))
            {
                device = GUISetting.Device.NANO_HOLTER;
            }
            
            while (!mctFound && i < fileLines.Length)
            {
                if (fileLines[i++].Contains(".arrhythmia_min_duration"))
                {
                    mctFound = true;
                    device = GUISetting.Device.PRO_MCT;
                }
            }
        
            return device;
        }
        
        private void SaveSettings(string path)
        {
            string tempFileName = @System.IO.Path.GetTempPath() + "settings.txt";
            string finalFileName = path;
            bool pass = true;
        
            String[] line = { "" };
        
            System.IO.File.WriteAllText(tempFileName, "#Generated By Settings Generator GUI\n");
            System.IO.File.WriteAllText(tempFileName, "file.firmware_version = " + fw_version + "\n");


            for (int i = 0; i < settingCount; i++)
            {
                if (ValidateSetting(i))
                {
                    String val = ReadSetting(i);
                    
                    if (!deviceSettings[i].GetName().StartsWith("."))
                    {
                        //If the setting name doesn't start with a period, it's a generator flag
                        if (deviceSettings[i].GetName().StartsWith("-rtc_tz_en"))
                        {
                            enable_timezone = val.StartsWith("1");
                            Properties.Settings.Default["TimezoneEnabled"] = enable_timezone;
                            Properties.Settings.Default.Save();
                        }
                    }
                    else if (val != "" || deviceSettings[i].GetName().StartsWith(".patient_id"))
                    {
                        bool valid = true;

                        // Don't write diary settings past the diary count
                        if ((deviceSettings[i].GetName().StartsWith(".symptom_entry") && 
                            DiaryNumber(deviceSettings[i].GetName()) > symptom_count) ||
                            (deviceSettings[i].GetName().StartsWith(".activity_entry") && 
                            DiaryNumber(deviceSettings[i].GetName()) > activity_count))
                        {
                            valid = false;
                        }

                        if (valid)
                        {
                            line[0] = "setting" + deviceSettings[i].GetName() + val;
                            File.AppendAllLines(tempFileName, line);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Invalid input on setting: " + deviceSettings[i].GetDisplayName(),
                                    "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    pass = false;
                    break;
                }
            }

            if (pass)
            {
                String descriptor;

                if (device == GUISetting.Device.NANO_HOLTER)
                {
                    descriptor = "HPR  ";
                }
                else
                {
                    descriptor = "H3R  ";
                }

                TxtToTzs.ConvertFile(tempFileName, finalFileName, descriptor);
            }
        }
        
        private bool ValidateSetting(int settingNumber)
        {
            GUISetting s = deviceSettings[settingNumber];
            bool valid = true;

            if ((s.GetGroup() == GUISetting.Group.Symptoms ||
                 s.GetGroup() == GUISetting.Group.Activities) &&
                 s.GetSettingType() == GUISetting.Type.INT)
            {
                valid = true;
            }
            else if (s.GetSettingType() == GUISetting.Type.STRING)
            {
                String val = settingForms[settingNumber].Entry.Text;
        
                if (val.Length > s.GetMaxLen())
                {
                    valid = false;
                }
        
            }
            else if (s.GetSettingType() == GUISetting.Type.INT)
            {
                String val = settingForms[settingNumber].Entry.Text;
                int numericVal;
        
                if (val != "")
                {
                    valid = int.TryParse(val, out numericVal);
        
                    if (valid && (numericVal < s.GetMinValue() || numericVal > s.GetMaxValue()))
                    {
                        valid = false;
                    }
                } 
            }
        
            return valid;
        }
        
        private string ReadSetting(int settingNumber)
        {
            GUISetting s = deviceSettings[settingNumber];
            string val;

            if ((s.GetGroup() == GUISetting.Group.Symptoms ||
                 s.GetGroup() == GUISetting.Group.Activities) &&
                 s.GetSettingType() == GUISetting.Type.INT)
            {
                val = "" + ((NumericUpDown)settingForms[settingNumber].Entry).Value;
            }
            else if (s.GetSettingType() == GUISetting.Type.STRING || s.GetSettingType() == GUISetting.Type.INT)
            {
                val = settingForms[settingNumber].Entry.Text;
            }
            else if (((CheckBox)settingForms[settingNumber].Entry).CheckState == CheckState.Checked)
            {
                val = "1";
            }
            else if (((CheckBox)settingForms[settingNumber].Entry).CheckState == CheckState.Unchecked)
            {
                val = "0";
            }
            else
            {
                val = "";
            }
        
            return val;
        }
        
        private bool SetSetting(String settingName, String settingValue)
        {
            bool settingFound = false;
            bool success = false;
            int i = 0;

            while (!settingFound && i < deviceSettings.Count)
            {
                GUISetting s = deviceSettings[i];

                if (s.GetName() == settingName)
                {
                    settingFound = true;

                    if ((s.GetGroup() == GUISetting.Group.Symptoms ||
                         s.GetGroup() == GUISetting.Group.Activities) &&
                         s.GetSettingType() == GUISetting.Type.INT)
                    {
                        ((NumericUpDown)settingForms[i].Entry).Value = 1;
                        ((NumericUpDown)settingForms[i].Entry).Value = 0; //BECAUSE REASONS
                        ((NumericUpDown)settingForms[i].Entry).Value = int.Parse(settingValue);
                        success = true;
                    }
                    else if (s.GetSettingType() == GUISetting.Type.BOOLEAN)
                    {
                        if (settingValue == "0")
                        {
                            ((CheckBox)settingForms[i].Entry).CheckState = CheckState.Unchecked;
                            success = true;
                        }
                        else if (settingValue == "1")
                        {
                            ((CheckBox)settingForms[i].Entry).CheckState = CheckState.Checked;
                            success = true;
                        }
                    }
                    else
                    {
                        ((TextBox)settingForms[i].Entry).Text = settingValue;
        
                        if (ValidateSetting(i))
                        {
                            success = true;
                        }
                        else
                        {
                            ((TextBox)settingForms[i].Entry).Text = "";
                        }
                    }
                }

                i++;
            }
        
            return success;
        }

        private void SymptomCountChanged(Object sender, EventArgs e)
        {
            symptom_count = (int) ((NumericUpDown)sender).Value;

            for (int i = 0; i < deviceSettings.Count; i++)
            {
                GUISetting s = deviceSettings[i];
                String name = s.GetName();
                int diary_num = 0;

                if (name.StartsWith(".symptom_entry"))
                {
                    diary_num = DiaryNumber(name);

                    if (diary_num <= symptom_count)
                    {
                        ((TextBox)settingForms[i].Entry).ReadOnly = false;
                        settingForms[i].Title.ForeColor = System.Drawing.Color.Empty;
                        ((TextBox)settingForms[i].Entry).BackColor = Color.Empty;
                        ((TextBox)settingForms[i].Entry).ForeColor = Color.Empty;

                    }
                    else
                    {
                        settingForms[i].Title.ForeColor = System.Drawing.SystemColors.ControlDark;
                        ((TextBox)settingForms[i].Entry).BackColor = SystemColors.Control;
                        ((TextBox)settingForms[i].Entry).ForeColor = SystemColors.Control;
                        ((TextBox)settingForms[i].Entry).ReadOnly = true;
                    }
                }
            }
        }

        private void ActivityCountChanged(Object sender, EventArgs e)
        {
            activity_count = (int)((NumericUpDown)sender).Value;

            for (int i = 0; i < deviceSettings.Count; i++)
            {
                GUISetting s = deviceSettings[i];
                String name = s.GetName();
                int diary_num = 0;

                if (name.StartsWith(".activity_entry"))
                {
                    diary_num = DiaryNumber(name);

                    if (diary_num <= activity_count)
                    {
                        ((TextBox)settingForms[i].Entry).ReadOnly = false;
                        settingForms[i].Title.ForeColor = System.Drawing.Color.Empty;
                        ((TextBox)settingForms[i].Entry).BackColor = Color.Empty;
                        ((TextBox)settingForms[i].Entry).ForeColor = Color.Empty;

                    }
                    else
                    {
                        settingForms[i].Title.ForeColor = System.Drawing.SystemColors.ControlDark;
                        ((TextBox)settingForms[i].Entry).BackColor = SystemColors.Control;
                        ((TextBox)settingForms[i].Entry).ForeColor = SystemColors.Control;
                        ((TextBox)settingForms[i].Entry).ReadOnly = true;
                    }
                }
            }
        }

        private int DiaryNumber(String DiaryName)
        {
            return int.Parse(DiaryName.Replace(".activity_entry_", "").Replace(".symptom_entry_", "").Replace("=", ""));
        }

        private class SettingEntry
        {
            public TextBox Title;
            public Control Entry;

            public SettingEntry(TextBox title, Control entry)
            {
                Title = title;
                Entry = entry;
            }
        }

        void EjectDrive()
        {
            string path = driveRoot;
            path = path.Remove(path.Length - 1);

            if (!RemoveDriveTools.RemoveDrive(path))
            {
                AutoClosingMessageBox.Show("Please disconnect the device from your computer.", "Remove Drive", int.MaxValue);
            }
        }

        private void CloseCanceller(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (e.CloseReason == ToolStripDropDownCloseReason.ItemClicked)
            {
                e.Cancel = true;
            }
            else
            {
                Properties.Settings.Default["BasicEnabled"] = this.basicToolStripMenuItem.Checked;
                Properties.Settings.Default["AdvancedEnabled"] = this.advancedToolStripMenuItem.Checked;
                Properties.Settings.Default["NotificationEnabled"] = this.notificationToolStripMenuItem.Checked;
                Properties.Settings.Default["DiaryEnabled"] = this.patientDiaryToolStripMenuItem.Checked;
                Properties.Settings.Default["ArrhythmiaEnabled"] = this.arrhythmiaToolStripMenuItem.Checked;
                Properties.Settings.Default.Save();
            }

            ReloadTabs();
            PerformLayout();
        }

        private void ReloadTabs()
        {
            foreach (TabPage t in settingTabs)
            {
                GUISetting.Group g = (GUISetting.Group) Enum.Parse(typeof(GUISetting.Group), t.Name);
                if (tabControlSettings.Contains(t) && !g.Enabled())
                {
                    tabControlSettings.Controls.Remove(t);
                }
                else if (!tabControlSettings.Contains(t) && g.Enabled())
                {
                    tabControlSettings.Controls.Add(t);
                }
            }
        }

        private void PreferenceMenuLoad(object sender, EventArgs e)
        {

        }

        private void enabledTabsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }

    public class AutoClosingMessageBox
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        DialogResult _result;
        DialogResult _timerResult;
        AutoClosingMessageBox(string text, string caption, int timeout, MessageBoxButtons buttons = MessageBoxButtons.OK, DialogResult timerResult = DialogResult.None)
        {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                null, timeout, System.Threading.Timeout.Infinite);
            _timerResult = timerResult;
            using (_timeoutTimer)
                _result = MessageBox.Show(text, caption, buttons);
        }
        public static DialogResult Show(string text, string caption, int timeout, MessageBoxButtons buttons = MessageBoxButtons.OK, DialogResult timerResult = DialogResult.None)
        {
            return new AutoClosingMessageBox(text, caption, timeout, buttons, timerResult)._result;
        }
        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow("#32770", _caption); // lpClassName is #32770 for MessageBox
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
            _result = _timerResult;
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}
