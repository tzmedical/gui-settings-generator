﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Settings_Generator_GUI.settings_forms
{
    public partial class FileCopyDialog : Form
    {
        private System.ComponentModel.BackgroundWorker copyWorker;

        private static String src;
        private static String dst;

        private static long totalBytes;
        private static long currentBytes;
        private static long currentPercent;

        public FileCopyDialog(String source, String dest)
        {
            InitializeComponent();

            src = source;
            dst = dest;

            progressBar.Maximum = 100;
            progressLabel.Text = "Calculating Size";

            copyWorker = new System.ComponentModel.BackgroundWorker();
            copyWorker.WorkerReportsProgress = true;
            copyWorker.WorkerSupportsCancellation = false;
            copyWorker.DoWork += new DoWorkEventHandler(workerDoWork);
            copyWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(workerWorkDone);
            copyWorker.ProgressChanged += new ProgressChangedEventHandler(workerProgressUpdate);

            copyWorker.RunWorkerAsync();

            //DirectoryCopy(source, dest, true);
            //Directory.Delete(Path.Combine(dest, "System Volume Information"), true);
        }

        private void workerDoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(src);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: " + src);
            }

            totalBytes = DirSize(dir);
            currentBytes = 0;
            currentPercent = -1;

            DirectoryCopy(src, dst, worker);
            Directory.Delete(Path.Combine(dst, "System Volume Information"), true);
        }

        private void workerWorkDone(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void workerProgressUpdate(object sender, ProgressChangedEventArgs e)
        {
            this.progressLabel.Text = e.ProgressPercentage + "% Copied";
            this.progressBar.Value = e.ProgressPercentage;

            progressLabel.Invalidate();
            progressBar.Invalidate();
        }

        private void DirectoryCopy(string sourceDirName, string destDirName, BackgroundWorker worker)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);

                currentBytes += file.Length;

                int percentProgress = (int) (100 * (float)currentBytes / (float)totalBytes); 

                if (percentProgress > currentPercent)
                {
                    currentPercent = percentProgress;
                    worker.ReportProgress(percentProgress);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, worker);
            }
        }

        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }  
    }
}
