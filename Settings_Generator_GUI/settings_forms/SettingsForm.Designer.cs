﻿using Microsoft.WindowsAPICodePack.Dialogs;

namespace Settings_Generator_GUI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.deviceSerial = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.saveFD = new System.Windows.Forms.SaveFileDialog();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.updateButton = new System.Windows.Forms.Button();
            this.preferenceMenuStrip = new System.Windows.Forms.MenuStrip();
            this.preferenceMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.enabledTabsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrhythmiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientDiaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferenceMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // deviceSerial
            // 
            this.deviceSerial.BackColor = System.Drawing.SystemColors.Control;
            this.deviceSerial.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.deviceSerial.Location = new System.Drawing.Point(13, 27);
            this.deviceSerial.Name = "deviceSerial";
            this.deviceSerial.ReadOnly = true;
            this.deviceSerial.Size = new System.Drawing.Size(259, 13);
            this.deviceSerial.TabIndex = 0;
            this.deviceSerial.Text = "Device Settings";
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Location = new System.Drawing.Point(230, 433);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(95, 23);
            this.browseButton.TabIndex = 5;
            this.browseButton.Text = "Retrieve Data";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // saveFD
            // 
            this.saveFD.DefaultExt = "tzs";
            this.saveFD.Filter = "Trident Settings Files (*.tzs)|*.tzs|All files (*.*)|*.*";
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlSettings.Location = new System.Drawing.Point(13, 51);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(424, 376);
            this.tabControlSettings.TabIndex = 7;
            // 
            // updateButton
            // 
            this.updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.updateButton.Location = new System.Drawing.Point(331, 433);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(106, 23);
            this.updateButton.TabIndex = 8;
            this.updateButton.Text = "Update Device";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // preferenceMenuStrip
            // 
            this.preferenceMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferenceMenu});
            this.preferenceMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.preferenceMenuStrip.Name = "preferenceMenuStrip";
            this.preferenceMenuStrip.Size = new System.Drawing.Size(449, 24);
            this.preferenceMenuStrip.TabIndex = 10;
            this.preferenceMenuStrip.Text = "preferenceMenu";
            // 
            // preferenceMenu
            // 
            this.preferenceMenu.BackColor = System.Drawing.SystemColors.ControlLight;
            this.preferenceMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.preferenceMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enabledTabsToolStripMenuItem});
            this.preferenceMenu.Name = "preferenceMenu";
            this.preferenceMenu.Size = new System.Drawing.Size(80, 20);
            this.preferenceMenu.Text = "Preferences";
            // 
            // enabledTabsToolStripMenuItem
            // 
            this.enabledTabsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.basicToolStripMenuItem,
            this.advancedToolStripMenuItem,
            this.notificationToolStripMenuItem,
            this.arrhythmiaToolStripMenuItem,
            this.patientDiaryToolStripMenuItem});
            this.enabledTabsToolStripMenuItem.Name = "enabledTabsToolStripMenuItem";
            this.enabledTabsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.enabledTabsToolStripMenuItem.Text = "Enabled Tabs";
            // 
            // basicToolStripMenuItem
            // 
            this.basicToolStripMenuItem.CheckOnClick = true;
            this.basicToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.basicToolStripMenuItem.DoubleClickEnabled = true;
            this.basicToolStripMenuItem.Name = "basicToolStripMenuItem";
            this.basicToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.basicToolStripMenuItem.Text = "Basic";
            this.basicToolStripMenuItem.ToolTipText = "Basic settings include Study Length, Patient ID, and Pacemaker Detection.";
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.CheckOnClick = true;
            this.advancedToolStripMenuItem.DoubleClickEnabled = true;
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.advancedToolStripMenuItem.Text = "Advanced";
            // 
            // notificationToolStripMenuItem
            // 
            this.notificationToolStripMenuItem.CheckOnClick = true;
            this.notificationToolStripMenuItem.DoubleClickEnabled = true;
            this.notificationToolStripMenuItem.Name = "notificationToolStripMenuItem";
            this.notificationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.notificationToolStripMenuItem.Text = "Notification";
            // 
            // arrhythmiaToolStripMenuItem
            // 
            this.arrhythmiaToolStripMenuItem.CheckOnClick = true;
            this.arrhythmiaToolStripMenuItem.DoubleClickEnabled = true;
            this.arrhythmiaToolStripMenuItem.Name = "arrhythmiaToolStripMenuItem";
            this.arrhythmiaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.arrhythmiaToolStripMenuItem.Text = "Arrhythmia";
            // 
            // patientDiaryToolStripMenuItem
            // 
            this.patientDiaryToolStripMenuItem.CheckOnClick = true;
            this.patientDiaryToolStripMenuItem.DoubleClickEnabled = true;
            this.patientDiaryToolStripMenuItem.Name = "patientDiaryToolStripMenuItem";
            this.patientDiaryToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.patientDiaryToolStripMenuItem.Text = "Patient Diary";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 461);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.tabControlSettings);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.deviceSerial);
            this.Controls.Add(this.preferenceMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(465, 500);
            this.MinimumSize = new System.Drawing.Size(465, 500);
            this.Name = "SettingsForm";
            this.Text = "Trident Settings Generator";
            this.Load += new System.EventHandler(this.PreferenceMenuLoad);
            this.preferenceMenuStrip.ResumeLayout(false);
            this.preferenceMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.TextBox deviceSerial;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.SaveFileDialog saveFD;
        private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.MenuStrip preferenceMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem preferenceMenu;
        private System.Windows.Forms.ToolStripMenuItem enabledTabsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem basicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrhythmiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientDiaryToolStripMenuItem;
    }
}