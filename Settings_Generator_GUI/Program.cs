﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Settings_Generator_GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (scanner == null)
            {
                scanner = new DeviceScanner();
                scanner.Show();
                scanner.Hide();
            }

            Application.Run();
        }

        private static DeviceScanner scanner = null;
    }
}
