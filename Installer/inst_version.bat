@ECHO OFF

setlocal ENABLEDELAYEDEXPANSION

for /f %%a in ('git describe') do (
  set ver=%%a-0
)

@echo | set /p="cscript.exe //nologo ../WiRunSQL.vbs trident_settings_installer.msi "UPDATE `Property` SET `Property`.`Value`='"

for /f "tokens=1 delims=v-" %%a in ('git describe') do (
  @echo | set /p="			%%a."
)
for /f "tokens=2 delims=-" %%a in ('git describe') do (
  @echo | set /p="%%a";
)
@echo ' WHERE `Property`.`Property`='ProductVersion'"

endlocal
